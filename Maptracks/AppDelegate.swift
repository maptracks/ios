//
//  AppDelegate.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import Firebase
import Mixpanel
import GoogleMaps
import GooglePlaces
import DeviceCheck

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var auth = SPTAuth.defaultInstance()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        // uncomment to enable persistence
        // Firestore.firestore().settings.isPersistenceEnabled = true
        GMSServices.provideAPIKey(GMSettings.SERVICES_API_KEY)
        GMSPlacesClient.provideAPIKey(GMSettings.SERVICES_API_KEY)
        if let auth = auth {
            auth.redirectURL = URL(string: "maptracks://spotify-login")
            auth.sessionUserDefaultsKey = "current session"
            auth.tokenSwapURL = URL(string: "https://maptracks.me/api/swap")
            auth.tokenRefreshURL = URL(string: "https://maptracks.me/api/refresh")
            auth.requestedScopes = [SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope, SPTAuthUserLibraryReadScope, SPTAuthUserReadPrivateScope, SPTAuthPlaylistReadCollaborativeScope, MTSPT.MTSPTAuthCurrentlyPlayingScope, MTSPT.MTSPTAuthRecentlyPlayedScope]
        }
        let envVars = ProcessInfo.processInfo.environment
        if let env = envVars["Maptracks_Env"] {
            if env == "production" {
                Mixpanel.initialize(token: MXSettings.RELEASE_TOKEN)
            } else {
                Mixpanel.initialize(token: MXSettings.DEV_TOKEN)
            }
        } else {
            Mixpanel.initialize(token: MXSettings.DEV_TOKEN)
        }
        Mixpanel.mainInstance().identify(distinctId: MXSender.getDistinctId())
        return true
    }
    
    func initSpotifyAuth() {
        let auth = SPTAuth.defaultInstance()
        
        if let session = auth?.session {
            if session.isValid() {
                MTSPTAudioController.only.initAudioStreaming()
                MTSpotify.only.accessToken = session.accessToken
            } else if (auth?.hasTokenRefreshService)! {
                auth?.renewSession(auth?.session, callback: { error, session in
                    if let err = error {
                        print(err.localizedDescription)
                    }
                    
                    if let sess = session {
                        auth?.session = sess
                        MTSPTAudioController.only.initAudioStreaming()
                        MTSpotify.only.accessToken = sess.accessToken
                    }
                })
            }
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let auth = auth {
            if auth.canHandle(url) {
                auth.handleAuthCallback(withTriggeredAuthURL: url, callback: { error, session in
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    let defaults = UserDefaults.standard
                    if let session = session {
                        let sessionData = NSKeyedArchiver.archivedData(withRootObject: session)
                        defaults.set(sessionData, forKey: "SpotifySession")
                        defaults.synchronize()
                        auth.session = session
                        MTSpotify.only.accessToken = session.accessToken
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "spotifyLoginSuccess"), object: nil)
                    }
                })
                return true
            }
        }
        return false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


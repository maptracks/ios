//
//  PlaybackButton.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/6/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class PlaybackButton: UIButton {
    // referenced in the maptrack table vc to identify the track
    var trackIndex: Int!
    var track: Track?
    var isPlaying: Bool = false

    class func instanceFromNib(x: CGFloat, y: CGFloat) -> PlaybackButton {
        let me = UINib(nibName: "PlaybackButton", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PlaybackButton

        me.frame = CGRect(x: x, y: y, width: Settings.BUTTON_SIZE, height: Settings.BUTTON_SIZE)
        me.layer.shadowOpacity = Settings.BUTTON_SHADOW_OPACITY
        me.imageView?.contentMode = .scaleAspectFit
        me.setImage(#imageLiteral(resourceName: "play-alpha"), for: .normal)
        me.imageEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8)
        
        return me
    }
    
    func togglePlaying() {
        isPlaying = !isPlaying
        if isPlaying {
            transitionStyles(newImage: #imageLiteral(resourceName: "stop-alpha"), newColor: MTColor.pink)
        } else {
            transitionStyles(newImage: #imageLiteral(resourceName: "play-alpha"), newColor: MTColor.green)
        }
    }
    
    func transitionStyles(newImage: UIImage, newColor: UIColor) {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        }, completion: { _ in
            self.setImage(newImage, for: .normal)
            UIView.animate(withDuration: 0.2, animations: {
                self.alpha = 1.0
            })
        })
    }
    
    func setClear() {
        self.backgroundColor = UIColor.clear
        self.alpha = 0.8
    }
}

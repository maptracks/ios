//
//  GradientLayer.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/18/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

@IBDesignable class TopdownGradientView: UIView {
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.black
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
    }
}


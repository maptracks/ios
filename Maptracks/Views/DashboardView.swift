//
//  DashboardViewController.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import CircleMenu

protocol DashboardViewDelegate {
    func displayMapController()
    func displayConnectController()
    func displayHistoryController()
    func logout()
}

class DashboardView: UIView {
    
    var menuButton: MenuButton!
    
    var delegate: DashboardViewDelegate?
    
    var map: NavButtonView!
    var settings: NavButtonView!
    var history: NavButtonView!
    
    class func instanceFromNib(frame: CGRect) -> DashboardView {
        let me = UINib(nibName: "DashboardView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DashboardView
        me.frame = frame
        me.alpha = 0
        return me
    }
    
    func initUi() {
        setupMenuButtons()
        display()
    }
    
    func display() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        })
    }
    
    func remove() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    func setupMenuButtons() {
        let halfWidth =  frame.width / 2
        let halfHeight = frame.height / 2
        map = NavButtonView.instanceFromNib(frame: CGRect(x: 0, y: 0, width: halfWidth, height: halfHeight), text: "Map Tracks", color: MTColor.green, image: #imageLiteral(resourceName: "music-big"))
        settings = NavButtonView.instanceFromNib(frame: CGRect(x: halfWidth, y: 0, width: halfWidth, height: halfHeight), text: "Settings", color: MTColor.pink, image: #imageLiteral(resourceName: "settings-big"))
        history = NavButtonView.instanceFromNib(frame: CGRect(x: halfWidth, y: halfHeight, width: halfWidth, height: halfHeight), text: "History", color: MTColor.purple, image: #imageLiteral(resourceName: "history-big"))
        let logout = NavButtonView.instanceFromNib(frame: CGRect(x: 0, y: halfHeight, width: halfWidth, height: halfHeight), text: "Logout", color: MTColor.blue, image: #imageLiteral(resourceName: "lock-big"))
        map.button.addTarget(self, action: #selector(mapAction), for: .touchUpInside)
        settings.button.addTarget(self, action: #selector(musicAction), for: .touchUpInside)
        history.button.addTarget(self, action: #selector(historyAction), for: .touchUpInside)
        logout.button.addTarget(self, action: #selector(logoutAction), for: .touchUpInside)
        addSubview(map)
        addSubview(settings)
        addSubview(history)
        addSubview(logout )
    }
    
    @objc func historyAction() {
        displayHistoryController()
    }
    
    @objc func musicAction() {
        displayConnectController()
    }
    
    @objc func mapAction() {
        displayMapController()
    }
    
    @objc func logoutAction() {
        logout()
    }
}

extension DashboardView: CircleMenuDelegate {
    func initMenuButton() {
        let yPos = (frame.size.height / 2) - 25
        let xPos = (frame.size.width / 2) - 25
        menuButton = MenuButton.instanceFromNib(frame: CGRect(x: xPos, y: yPos, width: 50, height: 50))
        menuButton.delegate = self
        addSubview(menuButton)
    }
    
    func circleMenu(_ circleMenu: CircleMenu, willDisplay button: UIButton, atIndex: Int) {
        switch (atIndex) {
        case 1:
            button.backgroundColor = MTColor.blue
            button.setImage(UIImage(named: "historyButton"), for: .normal)
            // TODO: an action for this option
            break
        case 2:
            button.backgroundColor = MTColor.green
            button.setImage(UIImage(named: "map"), for: .normal)
            break
        case 3:
            button.backgroundColor = MTColor.purple
            button.setImage(UIImage(named: "logout"), for: .normal)
            break
        default:
            button.backgroundColor = MTColor.pink
            button.setImage(UIImage(named: "music"), for: .normal)
            break
        }
    }
    
    func circleMenu(_ circleMenu: CircleMenu, buttonDidSelected button: UIButton, atIndex: Int) {
        switch (atIndex) {
        case 1:
            displayHistoryController()
            break
        case 2:
            displayMapController()
            break
        case 3:
            logout()
            break
        default:
            displayConnectController()
            break
        }
    }
}

extension DashboardView {
    @objc func displayHistoryController() {
        delegate?.displayHistoryController()
    }
    
    @objc func displayMapController() {
        delegate?.displayMapController()
    }
    
    @objc func displayConnectController() {
        delegate?.displayConnectController()
    }
    
    @objc func logout() {
        MXSender.trackLogoutEvent()
        delegate?.logout()
    }
}



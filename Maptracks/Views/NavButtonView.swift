//
//  NavButtonView.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/30/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class NavButtonView: UIView {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    
    class func instanceFromNib(frame: CGRect, text: String, color: UIColor, image: UIImage?) -> NavButtonView {
        let me = UINib(nibName: "NavButton", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavButtonView
        
        me.frame = frame
        me.label.text = text
        me.button.setTitle(text, for: .normal)
        me.backgroundColor = color
        me.button.setTitleColor(color, for: .normal)
        me.iconView.image = image
        
        return me
    }
}

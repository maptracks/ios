//
//  EditMaptrackView.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/11/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class EditMaptrackView: UIView {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var uploadImageButton: UIButton!
    @IBOutlet weak var commentView: UITextView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var remainingCharsLabel: UILabel!
    
    var maptrack: Maptrack!
    
    class func instanceFromNib(frame: CGRect) -> EditMaptrackView {
        let me = UINib(nibName: "EditMaptrackView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EditMaptrackView
        
        me.frame = CGRect(x: frame.width, y: frame.origin.y, width: frame.width, height: frame.height)
        me.alpha = 0.0
        me.commentView.delegate = me

        return me
    }
    
    
    func slideInLeft(callback: @escaping (Bool) -> Void) {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
            self.frame = CGRect(x: 0, y: self.frame.origin.y, width: self.frame.width, height: self.frame.height)
        }, completion: callback)
    }
    
    func slideOutRight(callback: @escaping () -> Void) {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
            self.frame = CGRect(x: self.frame.width, y: self.frame.origin.y, width: self.frame.width, height: self.frame.height)
        }, completion: {_ in
            self.removeFromSuperview()
            callback()
        })
    }
    
    @IBAction func uploadImageAction(_ sender: Any) {
    
    }
}

extension EditMaptrackView: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if let text = textView.text, text == Settings.EDIT_PROMPT_TEXT {
            textView.text = ""
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Settings.EDIT_PROMPT_TEXT
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let maxLength = 140
        let positiveColor = UIColor.init(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
        if let text = textView.text {
            let remaining = maxLength - text.count
            remainingCharsLabel.text = String(remaining)
            if remaining < 0 {
                remainingCharsLabel.textColor = MTColor.pink
            } else {
                remainingCharsLabel.textColor = positiveColor
            }
        }
    }
}





//
//  GradientShadowView.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/12/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class GradientShadowView: UIView {
    let gradient = CAGradientLayer()
    
    override func awakeFromNib() {
        setupGradientView()
        setupShadow()
    }
    
    func setupGradientView() {
        gradient.frame = self.bounds
        gradient.colors = [MTColor.background.cgColor, MTColor.backgroundClear.cgColor]
        gradient.startPoint = CGPoint.zero
        gradient.endPoint = CGPoint(x: 0, y: 1)
        gradient.locations = [0.8, 1.0]
        self.layer.addSublayer(gradient)
    }
    
    func setupShadow() {
        layer.shadowOpacity = 0.3
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSize(width: 0, height: 5.0)
    }
}

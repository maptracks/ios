//
//  TableHeader.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/11/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class TableHeader: UITableViewCell {
    
    @IBOutlet weak var backButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

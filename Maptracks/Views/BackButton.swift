//
//  BackButton.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/9/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class BackButton: UIButton {
    var useBackground = true {
        didSet {
            setBackground()
        }
    }

    class func instanceFromNib(x: CGFloat, y: CGFloat?) -> BackButton {
        let me = UINib(nibName: "BackButton", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BackButton
        let newX = x - (Settings.BUTTON_SIZE / 2)
        me.frame = CGRect(x: newX, y: y ?? Settings.BUTTON_Y, width: Settings.BUTTON_SIZE, height: Settings.BUTTON_SIZE)
        me.layer.cornerRadius = Settings.BUTTON_RADIUS
        me.layer.shadowOpacity = Settings.BUTTON_SHADOW_OPACITY
        me.layer.shadowColor = UIColor.darkGray.cgColor
        me.layer.shadowRadius = Settings.BUTTON_SHADOW_RADIUS
        me.layer.shadowOffset = Settings.BUTTON_SHADOW_OFFSET
        return me
    }
    
    func setBackground() {
        if !useBackground {
            backgroundColor = UIColor.clear
        } else {
            backgroundColor = MTColor.blue
        }
    }
}

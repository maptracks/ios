//
//  StartView.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class StartView: UIView {
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    weak var delegate: StartViewDelegate?
    
    class func instanceFromNib(frame: CGRect) -> StartView {
        let me = UINib(nibName: "StartView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! StartView
        me.frame = frame
        me.alpha = 0.0
        return me
    }
    
    func display() {
        loginButton.layer.borderColor = MTColor.inputBorder.cgColor
        loginButton.layer.cornerRadius = 4
        loginButton.layer.borderWidth = 1
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        }, completion: nil)
    }
    
    func remove() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        delegate?.handleChangeTo(.signup)
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        delegate?.handleChangeTo(.login)
    }
    
}

protocol StartViewDelegate: NSObjectProtocol {
    func handleChangeTo(_ authMode: AuthMode)
}

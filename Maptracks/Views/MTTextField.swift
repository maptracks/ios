//
//  MTTextField.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class MTTextField: UITextField {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareUi()
    }
    
    func prepareUi() {
        backgroundColor = UIColor.clear
        textColor = UIColor.white
        layer.borderWidth = 1
        layer.borderColor = MTColor.inputBorder.cgColor
        layer.cornerRadius = 4
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 20, y: bounds.origin.y + 4, width: bounds.width - 40, height: bounds.height - 4)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 20, y: bounds.origin.y + 4, width: bounds.width - 40, height: bounds.height - 4)
    }
}

extension MTTextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set (newValue) {
            attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "", attributes: [NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
}

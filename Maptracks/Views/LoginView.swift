//
//  LoginView.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import CircleMenu
import Material

class LoginView: UIView, CircleMenuDelegate {
    @IBOutlet weak var backButton: FlatButton!
    @IBOutlet weak var loginButton: FlatButton!
    @IBOutlet weak var emailField: MTTextField!
    @IBOutlet weak var passField: MTTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    weak var delegate: LoginViewDelegate?
    
    class func instanceFromNib(frame: CGRect) -> LoginView {
        let me = UINib(nibName: "LoginView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LoginView
        me.frame = frame
        me.alpha = 0.0
        return me
    }
    
    func initUi() {
        prepareEmailField()
        preparePassField()
        prepareButtonStyles()
        prepareTapToEndEditing()
        display()
    }
    
    func display() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        })
    }
    
    func remove() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    @objc func handleScrollTap() {
        endEditing(true)
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        handleAuthenticate()
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        delegate?.handleSwitchToSignUpMode()
    }
}

extension LoginView {
    func handleAuthenticate() {
        if let email = emailField.text {
            if let password = passField.text {
                delegate?.authenticateUser(email: email, password: password)
            }
        }
    }
}

extension LoginView {
    func prepareEmailField() {
        emailField.delegate = self
    }
    
    func preparePassField() {
        passField.delegate = self
    }
    
    func prepareButtonStyles() {
        loginButton.layer.cornerRadius = 4
    }
    
    func prepareTapToEndEditing() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleScrollTap))
        contentView.addGestureRecognizer(tap)
    }
}

extension LoginView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var offset: CGFloat = 0
        
        if textField === emailField {
            offset = 60
        } else {
            offset = 80
        }
        
        scrollView.setContentOffset(CGPoint(x: 0, y: offset), animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField === passField {
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === emailField {
            return passField.becomeFirstResponder()
        }
        
        passField.endEditing(true)
        return true
    }
}

protocol LoginViewDelegate: NSObjectProtocol {
    func handleSwitchToSignUpMode()
    func authenticateUser(email: String, password: String)
}



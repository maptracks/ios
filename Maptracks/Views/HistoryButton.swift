//
//  HistoryButton.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class HistoryButton: UIButton {
    var isOn = false

    class func instanceFromNib(x: CGFloat) -> HistoryButton {
        let me = UINib(nibName: "HistoryButton", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! HistoryButton
        let newX = x - (Settings.BUTTON_SIZE / 2)
        me.frame = CGRect(x: newX, y: 12.0, width: Settings.BUTTON_SIZE, height: Settings.BUTTON_SIZE)
        me.layer.cornerRadius = Settings.BUTTON_RADIUS
        me.layer.shadowOpacity = Settings.BUTTON_SHADOW_OPACITY
        me.layer.shadowColor = UIColor.darkGray.cgColor
        me.layer.shadowRadius = Settings.BUTTON_SHADOW_RADIUS
        me.layer.shadowOffset = Settings.BUTTON_SHADOW_OFFSET
        return me
    }
    
    @objc func toggleOn() {
        isOn = !isOn
        if (isOn) {
            backgroundColor = MTColor.green
        } else {
            backgroundColor = MTColor.pink
        }
    }
}

//
//  AudioSnackbarView.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/6/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class AudioSnackbarView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var swipeView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    
    
    var track: Track!
    
    var playbackButton: PlaybackButton!
    
    class func instanceFromNib(y: CGFloat, width: CGFloat) -> AudioSnackbarView {
        let me = UINib(nibName: "AudioSnackbarView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AudioSnackbarView
        me.alpha = 0.0
        me.frame = CGRect(x: 0, y: y + Settings.SNACKBAR_HEIGHT, width: width, height: Settings.SNACKBAR_HEIGHT)
        me.setupShadow()

        return me
    }
    
    func setupLabels() {
        titleLabel.text = track.title
        artistLabel.text = track.artist
    }
    
    func setupShadow() {
        layer.shadowOpacity = 0.3
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSize(width: 0, height: -5.0)
    }
    
    func setupUserImage() {
        
    }
    
    func slideUp() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
            self.frame = CGRect(origin: CGPoint(x: self.frame.origin.x, y: self.frame.origin.y - Settings.SNACKBAR_HEIGHT), size: self.frame.size)
        })
    }
    
    func slideUpThen(callback: @escaping () -> Void) {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
            self.frame = CGRect(origin: CGPoint(x: self.frame.origin.x, y: self.frame.origin.y - Settings.SNACKBAR_HEIGHT), size: self.frame.size)
        }, completion: { _ in
            callback()
        })
    }
    
    func slideDown() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
            self.frame = CGRect(origin: CGPoint(x: self.frame.origin.x, y: self.frame.origin.y + Settings.SNACKBAR_HEIGHT), size: self.frame.size)
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    func slideDownThen(callback: @escaping () -> Void) {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
            self.frame = CGRect(origin: CGPoint(x: self.frame.origin.x, y: self.frame.origin.y + Settings.SNACKBAR_HEIGHT), size: self.frame.size)
        }, completion: { _ in
            self.removeFromSuperview()
            callback()
        })
    }
    
    func expand(toHeight height: CGFloat) {
        UIView.animate(withDuration: 0.2, animations: {
            self.frame = CGRect(origin: CGPoint(x: self.frame.origin.x, y: self.frame.origin.y - height), size: CGSize(width: self.frame.size.width, height: height))
        }, completion: nil)
    }
}

//
//  MaptrackTableView.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class MaptrackCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumImage: UIImageView!
    
    var playButton: PlaybackButton?
}

//
//  EditHistoryButton.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/10/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class EditHistoryButton: UIButton {
    var maptrack: Maptrack!
    var track: Track!
}

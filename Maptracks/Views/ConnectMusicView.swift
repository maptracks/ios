//
//  ConnectMusicView.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/13/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

protocol ConnectMusicViewDelegate {
    func goBack()
    func authenticateSpotify()
    func isSpotifyConnected() -> Bool
    func authenticateAppleMusic() -> Bool
    func isAppleMusicConnected() -> Bool
    func authenticateMusicApp()
    func isMusicAppConnected() -> Bool
}

class ConnectMusicView: UIView {
    @IBOutlet weak var spotifyButton: UIButton!
    @IBOutlet weak var connectAppleButton: UIButton!
    @IBOutlet weak var connectMusicAppButton: UIButton!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    
    
    var delegate: ConnectMusicViewDelegate?
    var auth = SPTAuth.defaultInstance()!
    
    var loginUrl: URL?
    var appleConnected = false

    var spotifyConnected = false {
        didSet {
            updateSpotifyButton(connected: spotifyConnected)
        }
    }
    
    var musicAppConnected: Bool = false {
        didSet {
            updateMusicAppButton(connected: musicAppConnected)
        }
    }

    class func instanceFromNib(frame: CGRect) -> ConnectMusicView {
        let me = UINib(nibName: "ConnectMusicView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ConnectMusicView
        me.alpha = 0
        me.frame = frame
        return me
    }
    
    override func awakeFromNib() {
//        setupBackButton()
    }
    
    func setupBackButton() {
        let yPos = settingsLabel.frame.origin.y
        let backBtn = BackButton.instanceFromNib(x: 24.0, y: yPos)
        backBtn.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        backBtn.useBackground = false
        backBtn.alpha = 0.8
        addSubview(backBtn)
    }
    
    // spotify button ui
    
    func setupSpotifyButton() {
        if spotifyConnected {
            updateSpotifyButtonConnected()
        } else {
            updateSpotifyButtonNotConnected()
        }
    }
    
    func updateSpotifyButton(connected: Bool) {
        if connected {
            updateSpotifyButtonConnected()
        } else {
            updateSpotifyButtonNotConnected()
        }
    }
    
    func updateSpotifyButtonConnected() {
        spotifyButton.setTitle("Spotify Connected", for: .normal)
        spotifyButton.backgroundColor = UIColor.clear
        spotifyButton.setTitleColor(MTColor.green, for: .normal)
    }
    
    func updateSpotifyButtonNotConnected() {
        spotifyButton.setTitle("Connect Spotify", for: .normal)
        spotifyButton.backgroundColor = MTColor.blue
        spotifyButton.setTitleColor(UIColor.white, for: .normal)
    }
    
    func updateMusicAppButton(connected: Bool) {
        if connected {
            updateMusicAppButtonConnected()
        } else {
            updateMusicAppButtonNotConnected()
        }
    }
    
    func updateMusicAppButtonConnected() {
        connectMusicAppButton.setTitle("iPhone Music App Connected", for: .normal)
        connectMusicAppButton.backgroundColor = UIColor.clear
        connectMusicAppButton.setTitleColor(MTColor.green, for: .normal)
    }
    
    func updateMusicAppButtonNotConnected() {
        connectMusicAppButton.setTitle("Connect iPhone Music App", for: .normal)
        connectMusicAppButton.backgroundColor = MTColor.blue
        connectMusicAppButton.setTitleColor(UIColor.white, for: .normal)
    }
    
    // apple music ui
    
    func setupAppleMusicButton() {
        if appleConnected {
            updateAppleButtonConnected()
        } else {
            updateAppleButtonNotConnected()
        }
    }
    
    func updateAppleMusicButton(connected: Bool) {
        if connected {
            updateAppleButtonConnected()
        } else {
            updateAppleButtonNotConnected()
        }
    }
    
    func updateAppleButtonConnected() {
        connectAppleButton.setTitle("Apple Music Connected", for: .normal)
        connectAppleButton.backgroundColor = UIColor.clear
        connectAppleButton.setTitleColor(MTColor.green, for: .normal)
    }
    
    func updateAppleButtonNotConnected() {
        connectAppleButton.setTitle("Connect Apple Music", for: .normal)
        connectAppleButton.backgroundColor = MTColor.blue
        connectAppleButton.setTitleColor(UIColor.white, for: .normal)
    }
    
    
    func display() {
        setupSpotifyButton()
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        })
    }
    
    func remove() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }

    @IBAction func connectSpotifyAction(_ sender: Any) {
        delegate?.authenticateSpotify()
    }
    
    @IBAction func connectAppleMusicAction(_ sender: Any) {
    }
    
    @IBAction func connectMusicAppAction(_ sender: UIButton) {
        delegate?.authenticateMusicApp()
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        goBack()
    }
    
}

extension ConnectMusicView {
    
}

extension ConnectMusicView {
    @objc func goBack() {
        delegate?.goBack()
    }
}

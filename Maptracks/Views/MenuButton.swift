//
//  MenuButton.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import CircleMenu

class MenuButton: CircleMenu {
    class func instanceFromNib(frame: CGRect) -> MenuButton {
        let me = UINib(nibName: "MenuButton", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MenuButton
        me.frame = frame
        me.layer.cornerRadius = me.frame.size.width / 2
        return me
    }
}

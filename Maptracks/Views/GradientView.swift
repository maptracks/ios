//
//  GradientView.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/12/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class GradientView: UIView {
    let gradient = CAGradientLayer()
    
    override func awakeFromNib() {
        setupGradientView()
    }
    
    func setupGradientView() {
        gradient.frame = self.bounds
        gradient.colors = [MTColor.background.cgColor, MTColor.backgroundClear.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint.zero
        gradient.locations = [0.8, 1.0]
        self.layer.addSublayer(gradient)
    }
}

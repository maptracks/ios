//
//  SignUpView.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import CircleMenu
import Material

class SignUpView: UIView, CircleMenuDelegate {
    @IBOutlet weak var signupButton: FlatButton!
    @IBOutlet weak var loginButton: FlatButton!
    @IBOutlet weak var nameField: MTTextField!
    @IBOutlet weak var emailField: MTTextField!
    @IBOutlet weak var passField: MTTextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var privacyPolicyBtn: UIButton!
    
    weak var delegate: SignUpViewDelegate?
    
    class func instanceFromNib(frame: CGRect) -> SignUpView {
        let me = UINib(nibName: "SignUpView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SignUpView
        me.frame = frame
        me.alpha = 0.0
        return me
    }
    
    func initUi() {
        prepareNameField()
        prepareEmailField()
        preparePassField()
        prepareButtonStyles()
        prepareTapToEndEditing()
        display()
    }
    
    func display() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        })
    }
    
    func remove() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    @objc func handleScrollTap() {
        endEditing(true)
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        handleUserCreate()
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        delegate?.handleSwitchToLoginMode()
    }
    
    @IBAction func showPrivacy(_ sender: Any) {
        delegate?.launchPrivacyPolicyBrowser()
    }
}

extension SignUpView {
    func handleUserCreate() {
        if let fullName = nameField.text {
            if let email = emailField.text {
                if let password = passField.text {
                    let validation = UserDataValidators.passwordResult(password: password)
                    if validation {
                        delegate?.createUser(fullName: fullName, email: email, password: password)
                    }
                }
            }
        }
    }
}

extension SignUpView {
    func prepareNameField() {
        nameField.delegate = self
    }
    
    func prepareEmailField() {
        emailField.delegate = self
    }
    
    func preparePassField() {
        passField.delegate = self
    }
    
    func prepareButtonStyles() {
        signupButton.layer.cornerRadius = 4
    }
    
    func prepareTapToEndEditing() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleScrollTap))
        contentView.addGestureRecognizer(tap)
    }
}

extension SignUpView: UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        var offset: CGFloat = 0
        
        if textField === nameField {
            offset = 40
        } else if textField === emailField {
            offset = 60
        } else {
            offset = 80
        }
        
        scrollView.setContentOffset(CGPoint(x: 0, y: offset), animated: true)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === nameField {
            return emailField.becomeFirstResponder()
        } else if textField === emailField {
            return passField.becomeFirstResponder()
        }
        
        passField.endEditing(true)
        return true
    }
    
    
}

protocol SignUpViewDelegate: NSObjectProtocol {
    func handleSwitchToLoginMode()
    func createUser(fullName: String, email: String, password: String)
    func launchPrivacyPolicyBrowser()
}

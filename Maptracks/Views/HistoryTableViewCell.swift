//
//  HistoryTableViewCell.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/10/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var editButton: EditHistoryButton!
    
    var maptrack: Maptrack!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.2).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ConnectButtonView.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/30/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class ConnectButtonView: UIView {
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var image: UIImageView!
    
    fileprivate var _service: String = ""
    
    private var _isConnected = false {
        didSet {
            updateAfterToggle()
        }
    }
    
    class func instanceFromNib(frame: CGRect, service: String) -> ConnectButtonView {
        let me = UINib(nibName: "ConnectButton", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ConnectButtonView

        me.frame = frame
        me.button.backgroundColor = MTColor.green
        me.image.image = #imageLiteral(resourceName: "link")

        return me
    }
    
    func toggleConnected(connected: Bool) {
        _isConnected = connected
    }
    
    func updateAfterToggle() {
        if _isConnected {
            UIView.animate(withDuration: 0.2, animations: {
                self.alpha = 0.0
            }, completion: { success in
                self.backgroundColor = UIColor.clear
                self.image.image = #imageLiteral(resourceName: "remove")
                UIView.animate(withDuration: 0.2, animations: {}, completion: { success in
                    self.alpha = 1.0
                })
            })
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.alpha = 0.0
            }, completion: { success in
                self.backgroundColor = MTColor.green
                self.image.image = #imageLiteral(resourceName: "link")
                UIView.animate(withDuration: 0.2, animations: {}, completion: { success in
                    self.alpha = 1.0
                })
            })
        }
    }
    
    @IBAction func removeAction(_ sender: Any) {
        print("remove")
    }
}

//
//  Firestore.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseAuth
import CoreLocation
import Mixpanel
import FirebaseRemoteConfig

class MTFirestore {
    static let only = MTFirestore()
    
    private var _store: Firestore!
    private let _settings = FirestoreSettings()
    private let _remoteConfig = RemoteConfig.remoteConfig()
    private let _remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true)
    
    private init() {
        _store = Firestore.firestore()
        _settings.isPersistenceEnabled = true
        _remoteConfig.configSettings = _remoteConfigSettings!
        _remoteConfig.setDefaults(fromPlist: "RemoteConfig")
        fetchDefaults()
    }
    
    private func fetchDefaults() {
        var timeout: Double = 3600
        if _remoteConfig.configSettings.isDeveloperModeEnabled {
            timeout = 0
        }
        _remoteConfig.fetch(withExpirationDuration: timeout, completionHandler: { status, error in
            if let err = error {
                print(err.localizedDescription)
            }
            if status == RemoteConfigFetchStatus.success {
                self._remoteConfig.activateFetched()
            }
        })
    }
    
    public var appleMusicToken: String {
        get {
            if let token = _remoteConfig[ConfigSettings.kAppleMusicTokenKey].stringValue {
                return token
            }
            return ""
        }
    }
    
    private var _uid: String? {
        get {
            return Auth.auth().currentUser?.uid
        }
    }
    
    public func newTrackId() -> String {
        return _store.collection("tracks").document().documentID
    }
    
    public func newRouteId() -> String {
        return _store.collection("routes").document().documentID
    }
    
    public func newMaptrackId() -> String {
        return _store.collection("maptracks").document().documentID
    }
    
    public func getUser(uid: String, callback: @escaping ([String: Any]?, Error?) -> Void) {
        var ref: DocumentReference? = nil
        ref = _store.collection("users").document(uid)
        ref?.getDocument(completion: { doc, error in
            if let err = error {
                print(err.localizedDescription)
                callback(nil, err)
            } else if let data = doc?.data() {
                callback(data, nil)
            } else {
                callback(nil, nil)
            }
        })
    }
    
    public func save(fireUser user: User, userData data: [String: Any]) {
        var ref: DocumentReference? = nil
        ref = _store.collection("users").document(user.uid)
        ref?.setData(data) { error in
            if let err = error {
                print(err.localizedDescription)
            } else {
                print("document added: \(ref!.documentID)")
            }
        }
    }
    
    public func save(playedTrack track: String, withLocations locations: [CLLocationCoordinate2D], withStartTime start: Date, withEndTime end: Date, withComment comment: String?, withImage image: String?) {
    }
    
    public func getRoutesForUser(callback: @escaping ([Maptrack]?, Error?) -> Void) {
        let ref = _store.collection("maptracks")
        let query = ref.whereField("user", isEqualTo: _uid as Any)
        query.getDocuments(completion: { snapshot, error in
            if let err = error {
                print(err.localizedDescription)
                callback(nil, err)
            } else {
                var maptracks = [Maptrack]()
                for doc in snapshot!.documents {
                    let maptrack = Maptrack(dictionary: doc.data())
                    maptracks.append(maptrack!)
                }
                                
                callback(maptracks, nil)
            }
        })
    }
    
    public func getTrack(forTrackId track: String, callback: @escaping (Track?, Error?) -> Void) {
        let ref = _store.collection("tracks").document(track)
        ref.getDocument(completion: { doc, error in
            if let err = error {
                print(err.localizedDescription)
                callback(nil, err)
            }
            if let data = doc?.data() {
                let track = Track(dictionary: data)
                callback(track!, nil)
            }
        })
    }
    
    public func getTracks(forTrackIds tracks: [String], callback: @escaping ([Track]?, Error?) -> Void) {
        let ref = _store.collection("tracks")
        var trackData = [Track]()
        let trackCount = tracks.count
        var currentIndex = 0

        for track in tracks {
            ref.document(track).getDocument(completion: { doc, error in
                currentIndex += 1
                if let err = error {
                    print(err.localizedDescription)
                    callback(nil, err)
                }
                if let data = doc?.data() {
                    let t = Track(dictionary: data)
                    trackData.append(t!)
                }
                if currentIndex == trackCount {
                    callback(trackData, nil)
                }
            })
        }
    }
    
    public func getMaptrack(forMaptrackId maptrack: String, callback: @escaping (Maptrack?, Error?) -> Void) {
        let ref = _store.collection("maptracks").document(maptrack)
        ref.getDocument(completion: { doc, error in
            if let err = error {
                print(err.localizedDescription)
                callback(nil, err)
            }
            if let data = doc?.data() {
                let mt = Maptrack(dictionary: data)
                callback(mt, nil)
            }
        })
    }
    
    public func getRoute(forRouteId route: String, callback: @escaping (Route?, Error?) -> Void) {
        let ref = _store.collection("routes").document(route)
        ref.getDocument(completion: { doc, error in
            if let err = error {
                print(err.localizedDescription)
                callback(nil, err)
            }
            if let data = doc?.data() {
                let track = Route(dictionary: data)
                callback(track, nil)
            }
        })
    }
    
    public func getNearbyRoutes(min: CLLocationCoordinate2D, max: CLLocationCoordinate2D, callback: @escaping ([Maptrack]?, Error?) -> Void) {
        let ref = _store.collection("maptracks")

        let maxGeo = GeoPoint(latitude: max.latitude, longitude: max.longitude)
        let minGeo = GeoPoint(latitude: min.latitude, longitude: min.longitude)

        let query = ref.whereField("midpoint", isGreaterThan: minGeo).whereField("midpoint", isLessThan: maxGeo)
       
        query.getDocuments(completion: { snapshot, error in
            if let err = error {
                print(err.localizedDescription)
                callback(nil, err)
            } else {
                var maptracks = [Maptrack]()
                for doc in snapshot!.documents {
                    let maptrack = Maptrack(dictionary: doc.data())
                    maptracks.append(maptrack!)
                }
                callback(maptracks, nil)
            }
        })
    }
    
    public func save(route: Route, id: String, maptrackId: String) {
        var ref: DocumentReference? = nil
        ref = _store.collection("routes").document(id)
        
        let routeCopy = saveable(route: route, maptrackId: maptrackId)
        
        ref?.setData(routeCopy) { error in
            if let err = error {
                print(err.localizedDescription)
            } else {
                print("saved route")
            }
        }
    }
    
    public func save(maptrack: Maptrack, id: String) {
        var ref: DocumentReference? = nil
        ref = _store.collection("maptracks").document(id)
        
        let maptrackCopy = saveable(maptrack: maptrack)
        
        ref?.setData(maptrackCopy) { error in
            if let err = error {
                print(err.localizedDescription)
            } else {
                MXSender.trackMaptrackEvent(id: id)
                print("saved maptrack")
            }
        }
    }
    
    public func save(track: Track, id: String, maptrackId: String) {
        var ref: DocumentReference? = nil
        ref = _store.collection("tracks").document(id)
        
        let trackCopy = saveable(track: track, maptrackId: maptrackId)
        
        ref?.setData(trackCopy) { error in
            if let err = error {
                print(err.localizedDescription)
            } else {
                print("saved track")
            }
        }
    }
    
    public func saveComment(maptrack: String, comment: String) {
        var ref: DocumentReference? = nil
        ref = _store.collection("maptracks").document(maptrack)
        
        ref?.updateData([
            "comment": comment
        ])
    }
    
    private func saveable(maptrack: Maptrack) -> [String: Any] {
        var maptrackCopy: [String: Any] = [String: Any]()
        
        maptrackCopy["id"] = maptrack.id
        maptrackCopy["comment"] = maptrack.comment
        maptrackCopy["image"] = maptrack.image
        maptrackCopy["start"] = maptrack.start
        maptrackCopy["end"] = maptrack.end
        maptrackCopy["track"] = maptrack.track
        maptrackCopy["user"] = maptrack.user
        maptrackCopy["route"] = maptrack.route
        maptrackCopy["midpoint"] = GeoPoint(latitude: maptrack.midpoint.lat, longitude: maptrack.midpoint.long)
        maptrackCopy["minpoint"] = GeoPoint(latitude: maptrack.minpoint.lat, longitude: maptrack.minpoint.long)
        maptrackCopy["maxpoint"] = GeoPoint(latitude: maptrack.maxpoint.lat, longitude: maptrack.maxpoint.long)

        
        return maptrackCopy
    }
    
    private func saveable(track: Track, maptrackId: String) -> [String: Any] {
        var trackCopy: [String: Any] = [String: Any]()
        
        trackCopy["id"] = track.id
        trackCopy["maptrack"] = maptrackId
        trackCopy["title"] = track.title
        trackCopy["artist"] = track.artist
        trackCopy["album"] = track.album
        trackCopy["audio"] = track.audio
        trackCopy["image"] = track.image
        trackCopy["spotify"] = track.spotify
        trackCopy["apple"] = track.apple
        trackCopy["isrc"] = track.isrc
        trackCopy["duration"] = track.duration
        
        return trackCopy
    }
    
    private func saveable(route: Route, maptrackId: String) -> [String: Any] {
        var routeCopy: [String: Any] = [String: Any]()
        
        var geopoints = [GeoPoint]()
        for pt in route.waypoints {
            let geo = GeoPoint(latitude: pt.lat, longitude: pt.long)
            geopoints.append(geo)
        }
        
        routeCopy["maptrack"] = maptrackId
        routeCopy["id"] = route.id
        routeCopy["waypoints"] = geopoints
        
        return routeCopy
    }
    
    public func geopointToObject(pt: GeoPoint) -> [String: Double] {
        return ["latitude": pt.latitude, "longitude": pt.longitude]
    }
    
    public func updateMaptrackImage(maptrackId id: String, downloadUrl: String) {
        var ref: DocumentReference? = nil
        ref = _store.collection("maptracks").document(id)
        ref?.updateData(["image": downloadUrl])
    }
}

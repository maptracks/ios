//
//  MTGoogleApi.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/7/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import Alamofire

class MTGoogleApi {
    static let only = MTGoogleApi()
    
    private init() {
        
    }
    
    func generatePathString(fromLocations locations: [CLLocationCoordinate2D]) -> String {
        var pathString = ""
        for i in 0..<locations.count {
            let loc = locations[i]
            pathString += String(loc.latitude)
            pathString += ","
            pathString += String(loc.longitude)
            if i < locations.count - 1 {
                pathString += "|"
            }
        }
        return pathString
    }
    
    func snapPointsToRoads(withLocations locations: [CLLocationCoordinate2D], callback: @escaping ([[String: Any]]?, Error?) -> Void) {
        let pathString = self.generatePathString(fromLocations: locations)
        
        let params: Parameters = [
            "path": pathString,
            "key": GApiSettings.API_KEY,
            "interpolate": true
        ]
        
        let apiUrl = GApiSettings.API_BASE.appending(GApiSettings.SNAP_ROADS)
        
        Alamofire.request(apiUrl, parameters: params).responseJSON(completionHandler: { response in
            if let respData = response.result.value as? [String: Any] {
                if let snappedData = respData["snappedPoints"] as? [[String: Any]]  {
                    callback(snappedData, nil)
                    return
                }
            }
            callback(nil, nil)
        })
    }
}

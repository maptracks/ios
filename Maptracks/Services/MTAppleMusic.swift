//
//  MTAppleMusic.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/2/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import StoreKit
import Alamofire

class MTAppleMusic {
    static let only = MTAppleMusic()
    
    let devToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IllNMzRMNjY5MzcifQ.eyJpc3MiOiJCOTQ1MkZFTVRGIiwiaWF0IjoxNTEyMjE1NTM5LCJleHAiOjE1MjUyNTgzMzl9.Y4gM-3c85rNqSVDZbQ6P7VfSW5n_RjdplOHQOla4cD7z-F9OWvENKgVbuqo1JioSrNteQAogCV23Zzkoq9f1Wg"
    
    var musicToken: String?
    
    private init() {
        let controller = SKCloudServiceController()
        controller.requestUserToken(forDeveloperToken: devToken, completionHandler: { token, error in
            if let err = error {
                print(err.localizedDescription)
            }
            if let t = token {
                self.musicToken = t
            }
        })
    }
    
    func getSongData() {
        let requestUrl = URL(string: "https://api.music.apple.com/v1/catalog/us/songs/203709340")
        let headers = [
            "Authorization": ("Bearer ").appending(devToken)
        ]
        
        Alamofire.request(requestUrl!, headers: headers).responseJSON(completionHandler: { response in
            if let data = response.result.value as? [String: Any] {
                print(data)
            }
        })
    }
}

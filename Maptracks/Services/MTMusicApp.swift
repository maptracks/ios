//
//  MTMusicApp.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/13/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import MediaPlayer

class MTMusicApp {
    static let only = MTMusicApp()
    
    private var _player: MPMusicPlayerController!
    private var _isAuthorized = false
    
    private init() {
        _player = MPMusicPlayerController.systemMusicPlayer
        checkAuthorization()
    }
    
    var isAuthorized: Bool {
        get {
            return _isAuthorized
        }
    }
    
    public func requestAuthorization(callback: @escaping (Bool) -> Void) {
        MPMediaLibrary.requestAuthorization({ authStatus in
            switch (authStatus) {
            case .authorized:
                self._isAuthorized = true
                break
            case .denied:
                self._isAuthorized = false
                break
            default:
                break
            }
            callback(self._isAuthorized)
        })
    }
    
    public func checkAuthorization() {
        let auth = MPMediaLibrary.authorizationStatus()
        switch (auth) {
        case .authorized:
            _isAuthorized = true
            break
        case .denied:
            _isAuthorized = false
            break
        case .notDetermined:
            _isAuthorized = false
            break
        default:
            break
        }
    }
    
    public func getCurrentlyPlaying() {
        print("get currently playing")
        if _player.playbackState == .playing {
            if let track = _player.nowPlayingItem {
                print(track)
                let trackId = MTFirestore.only.newTrackId()
                let maptrackId = MTFirestore.only.newMaptrackId()
                var t = Track(id: trackId, maptrack: maptrackId, title: track.title!, artist: track.albumArtist!, album: track.albumTitle!, audio: "", image: GApiSettings.DEFAULT_TRACK_IMAGE, spotify: "", apple: String(track.persistentID), isrc: "", duration: track.playbackDuration)
                
                if let artwork = track.artwork?.image(at: CGSize(width: 64, height: 64)) {
                    
                    DispatchQueue.global().async {
                        if let data = UIImagePNGRepresentation(artwork) {
                            MTStorage.only.uploadAlbumImage(data: data, track: t, callback: { url, error in
                                
                                DispatchQueue.main.async {
                                    if let err = error {
                                        print(err.localizedDescription)
                                        self.postCurrentlyPlayingNotification(track: t, progress: Double(track.bookmarkTime), timestamp: Double(Date().timeIntervalSince1970))
                                    }
                                    if let downloadUrl = url {
                                        t.image = downloadUrl
                                        self.postCurrentlyPlayingNotification(track: t, progress: Double(track.bookmarkTime), timestamp: Double(Date().timeIntervalSince1970))
                                    }
                                }
                            })
                        } else {

                            DispatchQueue.main.async {
                                self.postCurrentlyPlayingNotification(track: t, progress: Double(track.bookmarkTime), timestamp: Double(Date().timeIntervalSince1970))
                            }
                        }
                    }
                } else {
                    postCurrentlyPlayingNotification(track: t, progress: Double(track.bookmarkTime), timestamp: Double(Date().timeIntervalSince1970))
                }
            }
        }
    }
    
    private func postCurrentlyPlayingNotification(track: Track, progress: Double, timestamp: Double) {
        print("posting")
        let notification = Notification(name: Notification.Name.init(rawValue: "musicAppCurrentlyPlaying"), object: MTMusicApp.only, userInfo: [
            "track": track,
            "progress": progress,
            "timestamp": timestamp,
            "isPlaying": true
        ])
        NotificationCenter.default.post(notification)
    }
    
    
}

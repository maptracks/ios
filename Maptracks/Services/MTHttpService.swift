//
//  MTHttpService.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/6/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import Alamofire

struct MTHttpService {
    private static let GM_API_KEY = "AIzaSyDfoI519lCqWBLn2Lj4uHn3TFZqHvME1bA"
    private static let SNAP_BASE_URL = "https://roads.googleapis.com/v1/snapToRoads"
    
    static func getSnapToRoadsData(pathString: String, callback: @escaping (Data?, Error?) -> Void) {
        print(pathString)
        let params: Parameters = [
            "path": pathString,
            "key": GM_API_KEY
        ]
        
        Alamofire.request(SNAP_BASE_URL, parameters: params)
            .validate(statusCode: 200..<300)
            .responseData(completionHandler: { response in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        callback(data, nil)
                        break
                    }
                    callback(nil, nil)
                    break
                case .failure(let error):
                    callback(nil, error)
                    break
            }
        })
    }
    
    static func getSnapToRoadsJson(pathString: String, callback: @escaping ([[String: Any]]?, Error?) -> Void) {
        let params: Parameters = [
            "path": pathString,
            "key": GM_API_KEY
        ]
        
        Alamofire.request(SNAP_BASE_URL, parameters: params)
            .validate(statusCode: 200..<300)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    if let data = response.result.value as? [String: Any] {
                        if let snappedData = data["snappedPoints"] as? [[String: Any]]  {
                            callback(snappedData, nil)
                            break
                        }
                    }
                    callback(nil, nil)
                    break
                case .failure(let error):
                    callback(nil, error)
                    break
            }
        })
    }
}

//
//  MTLocManager.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import CoreLocation
import FirebaseFirestore

class MTLocManager: NSObject {
    static let only = MTLocManager()
    
    private var _locManager = CLLocationManager()
    
    private var _lastSavedGeo: CLLocationCoordinate2D?
    private var _pathLocations: [CLLocationCoordinate2D]!
    private var _isUpdating: Bool = false
    
    var delegate: MTLocManagerDelegate?
    
    var isUpdating: Bool {
        get {
            return _isUpdating
        }
    }

    private override init() {
        _lastSavedGeo = nil
        _pathLocations = [CLLocationCoordinate2D]()
    }
    
    func requestLocationPermissions() {
        _locManager.delegate = self
        _locManager.requestAlwaysAuthorization()
        _locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        _locManager.allowsBackgroundLocationUpdates = true
    }
    
    func stopUpdating() {
        _locManager.stopUpdatingLocation()
        _isUpdating = false
    }
    
    func startUpdating() {
        _locManager.startUpdatingLocation()
        _isUpdating = true
    }
}

extension MTLocManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch (status) {
        case .authorizedAlways:
            self.startUpdating()
            break
        case .authorizedWhenInUse:
            self.startUpdating()
            break
        case .denied:
            break
        case .notDetermined:
            break
        default:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newest = locations.last {
            delegate?.locationDidUpdate(newLocation: newest.coordinate)
            if _pathLocations.count <= 10 {
                _pathLocations.append(newest.coordinate)
            } else {
                delegate?.snapToRoads(withLocations: _pathLocations)
                let last = _pathLocations.last!
                _pathLocations = [last]
            }
        }
    }
}

protocol MTLocManagerDelegate {
    func locationDidUpdate(newLocation location: CLLocationCoordinate2D)
    func snapToRoads(withLocations: [CLLocationCoordinate2D])
}





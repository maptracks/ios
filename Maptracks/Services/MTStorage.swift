//
//  MTStorage.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/10/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import FirebaseStorage

class MTStorage {
    static let only = MTStorage()
    
    var _storage: StorageReference!
    
    private init() {
        _storage = Storage.storage().reference()
    }
    
    public func uploadTrackImage(data: Data, trackId: String, callback: @escaping (_ imageUrl: String?, _ error: Error?) -> Void) {
        let path = GApiSettings.FIRESTORASGE_TRACKS.appending(trackId).appending("/image.png")
        let imageRef = _storage.child(path)
        let meta = StorageMetadata()
        meta.contentType = "image/png"
        let _ = imageRef.putData(data, metadata: meta) { (metadata, error) in
            if let err = error {
                print(err.localizedDescription)
                callback(nil ,err)
            } else if let md = metadata {
                let downloadUrl = md.downloadURL()?.absoluteString
                callback(downloadUrl, nil)
            } else {
                callback(nil, nil)
            }
        }
    }
    
    public func uploadAlbumImage(data: Data, track: Track, callback: @escaping (_ imageUrl: String?, _ error: Error?) -> Void) {
        let pathname = StringUtils.makeAlbumImageFilename(album: track.album, artist: track.artist)
        let path = GApiSettings.FIRESTORASGE_TRACKS.appending(pathname).appending("/image.png")
        let imageRef = _storage.child(path)
        let meta = StorageMetadata()
        meta.contentType = "image/png"
        imageRef.downloadURL(completion: { url, error in
            if let _ = error {
                let _ = imageRef.putData(data, metadata: meta) { (metadata, error) in
                    if let err = error {
                        print(err.localizedDescription)
                        callback(nil ,err)
                    } else if let md = metadata {
                        let downloadUrl = md.downloadURL()?.absoluteString
                        callback(downloadUrl, nil)
                    } else {
                        callback(nil, nil)
                    }
                }
            } else if let url = url {
                callback(url.absoluteString, nil)
            }
        })
    }
    
    public func uploadMaptrackUserImage(data: Data, maptrackId: String, callback: @escaping (_ imageUrl: String?, _ error: Error?) -> Void) {
        let path = GApiSettings.FIRESTORAGE_MAPTRACKS.appending(maptrackId).appending("/image.png")
        let imageRef = _storage.child(path)
        let meta = StorageMetadata()
        meta.contentType = "image/png"
        let _ = imageRef.putData(data, metadata: meta) { (metadata, error) in
            if let err = error {
                print(err.localizedDescription)
                callback(nil ,err)
            } else if let md = metadata {
                let downloadUrl = md.downloadURL()?.absoluteString
                callback(downloadUrl, nil)
            } else {
                callback(nil, nil)
            }
        }
    }
}

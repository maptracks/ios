//
//  MTSpotify.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/18/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import Alamofire

class MTSpotify {
    static let only = MTSpotify()
    
    private var _accessToken: String = ""
    private var _username: String = ""
    private var _maptracksPlaylist: SPTPlaylistSnapshot?
    
    private init() {
        if let session = SPTAuth.defaultInstance().session {
            _accessToken = session.accessToken
        }
    }
    
    var accessToken: String {
        get {
            return _accessToken
        }
        set(newValue) {
            _accessToken = newValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var isConnected: Bool {
        get {
            if let session = SPTAuth.defaultInstance().session {
                return session.isValid()
            }
            return false
        }
    }
    
    func createPlaylist(name: String) {
        SPTUser.requestCurrentUser(withAccessToken: _accessToken) { (error, response) in
            if let err = error {
                print(err.localizedDescription)
            } else if let data = response as? SPTUser {
                self._username = data.canonicalUserName
                SPTPlaylistList.createPlaylist(withName: name, forUser: self._username, publicFlag: false, accessToken: self._accessToken, callback: { err, snapshot in
                    if let e = err {
                        print(e.localizedDescription)
                    } else if let snap = snapshot {
                        self._maptracksPlaylist = snap
                        UserDefaults.standard.set(true, forKey: "MaptracksPlaylistCreated")
                    }
                })
            } else if let data = response {
                print(data)
            }
        }
    }

    func getCurrentlyPlaying() {
        let requestUrl = URL(string: SPTSettings.API_BASE.appending(SPTSettings.CURRENTLY_PLAYING))
        let auth = ("Bearer ").appending(_accessToken)
        let headers: [String: String] = [
            "Authorization": auth
        ]
   
        Alamofire.request(requestUrl!, headers: headers).responseJSON(completionHandler: { response in
            var timestamp = Date().timeIntervalSince1970
            if let data = response.result.value as? [String: Any] {
                var progressMs: Double = 0
                var isPlaying = false

                if let progress = data["progress_ms"] as? Double {
                    progressMs = progress
                }

                if let playing = data["is_playing"] as? Bool {
                    isPlaying = playing
                }

                if let time = data["timestamp"] as? Double {
                    timestamp = time
                }

                if let item = data["item"] as? [String: Any] {
                    let trackId = MTFirestore.only.newTrackId()
                    let track = SPTDataExtractor.trackFromResponseItem(json: item, trackId: trackId)
                    let notification = Notification(name: Notification.Name.init(rawValue: "spotifyCurrentlyPlaying"), object: MTSpotify.only, userInfo: [
                        "track": track,
                        "progress": progressMs,
                        "isPlaying": isPlaying,
                        "timestamp": timestamp
                    ])
                    NotificationCenter.default.post(notification)
                }
            }
        })
    }
}

//
//  Downloader.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/9/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

extension NSError {
    static func generalParsingError(url: String) -> NSError {
        return NSError(domain: url, code: 400, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Error retrieving data", comment: "General parsing error description")])
    }
}

class MTDownloader {
    fileprivate static let imageCache = NSCache<NSString, UIImage>()
    
    static func cacheImage(_ image: UIImage?, atUrlString url: String) {
        if let image = image {
            imageCache.setObject(image, forKey: url as NSString)
        }
    }
    
    static func downloadImage(url: URL, callback: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            callback(cachedImage, nil)
        } else {
            MTDownloader.downloadData(url: url, callback: { data, error in
                if let err = error {
                    print(err.localizedDescription)
                    callback(nil, err)
                } else if let d = data, let image = UIImage(data: d) {
                    imageCache.setObject(image, forKey: url.absoluteString as NSString)
                    callback(image, nil)
                } else {
                    callback(nil, NSError.generalParsingError(url: url.absoluteString))
                }
            })
        }
    }
    
    fileprivate static func downloadData(url: URL, callback: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        let urlRequest = URLRequest(url: url)
        let task = URLSession(configuration: .default).dataTask(with: urlRequest, completionHandler: { data, response, error in
            callback(data, error)
        })
        task.resume()
    }
}

//
//  MTDefaults.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/15/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

class MTDefaults {
    static let only = MTDefaults()
    
    private var _defaults: UserDefaults!
    
    private init() {
        _defaults = UserDefaults.standard
    }
    
    func setSpotifyConnectionStatus(toUsing using: Bool) {
        _defaults.set(using, forKey: "USING_SPOTIFY")
    }
    
}

//
//  MTSessionData.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/9/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import FirebaseAuth

class MTSessionData {
    static let only = MTSessionData()
    
    // starting location set by first location update
    var initialLocation: CLLocationCoordinate2D?
    // millis of route start and end times
    var routeStart: Double!
    var routeEnd: Double!
    
    // waypoints on the current route
    var waypoints: [Waypoint] = [Waypoint]()
    // tracks from this session
    var tracks: [Track] = [Track]()
    // routes from this session
    var routes: [Route] = [Route]()
    // maptracks recorded
    var maptracks: [Maptrack] = [Maptrack]()
    
    // current updating routes
    var activeRoute: Route?
    // current listening track
    var activeTrack: Track?
    // time the track started in millis
    var trackStart: Double?
    // current position in active track
    var trackProgress: Double?
    
    // active user's id
    var uid: String!
    
    private var maptrackQueue = [Maptrack]()
    
    private init() {
        if let userId = Auth.auth().currentUser {
            uid = userId.uid
        }
        routeStart = 0
        routeEnd = 0
        initMaptracksQueue()
    }
    
    private func initMaptracksQueue() {
        if let playlistCreated = UserDefaults.standard.value(forKey: "MaptracksPlaylistCreated") as? Bool {
            if playlistCreated == false {
                MTSpotify.only.createPlaylist(name: "MaptracksQueue")
            }
        }
        if let ids = UserDefaults.standard.array(forKey: "MaptrackQueue") as? [String] {
            self.reAddToQueue(count: 0, ids: ids)
        }
    }
    
    private func reAddToQueue(count: Int, ids: [String]) {
        DispatchQueue.global(qos: .userInteractive).async {
            MTFirestore.only.getMaptrack(forMaptrackId: ids[count], callback: { maptrack, err in
                DispatchQueue.main.async {
                    if let mt = maptrack {
                        self.maptrackQueue.append(mt)
                    }
                    if count < ids.count - 1 {
                        self.reAddToQueue(count: count + 1, ids: ids)
                    } else {
                        print("done adding to queue")
                    }
                }
            })
        }
    }
    
    // MARK: - Public Getters
    
    var lastWaypoints: [Waypoint] {
        get {
            if let r = routes.last {
                return r.waypoints
            }
            return [Waypoint]()
        }
    }
    
    var activeWaypoints: [Waypoint] {
        get {
            if let r = activeRoute {
                return r.waypoints
            }
            return [Waypoint]()
        }
    }
    
    // MARK: - Public Methods
    
    public func setCurrentUser() {
        if let userId = Auth.auth().currentUser {
            uid = userId.uid
        } else {
            uid = ""
        }
    }
    
    public func clearMaptrackQueue() {
        maptrackQueue.removeAll()
    }
    
    public func addToMaptrackQueue(maptrack: Maptrack) {
        maptrackQueue.append(maptrack)

        let allQueuedIds: [String] = maptrackQueue.map({ item in
            return item.id
        })

        UserDefaults.standard.set(allQueuedIds, forKey: "MaptrackQueue")
    }
    
    public func playNextTrackInQueue() {
        if let next = maptrackQueue.first {
            DispatchQueue.global(qos: .userInitiated).async {
                MTFirestore.only.getTrack(forTrackId: next.track, callback: { track, error in
                    if let err = error {
                        print(err.localizedDescription)
                    } else if let t = track {
                        DispatchQueue.main.async {
                            MTSPTAudioController.only.startTrack(track: t)
                            self.maptrackQueue.removeFirst()
                        }
                    }
                })
            }
        }
    }
}




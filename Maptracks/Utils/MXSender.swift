//
//  MXSender.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/10/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import Mixpanel

struct MXSender {
    static func getDistinctId() -> String {
        return Mixpanel.mainInstance().distinctId
    }
    
    static func setSpotifyConnectedProperty(isConnected: Bool) {
        Mixpanel.mainInstance().people.set(properties: ["Spotify Connected": isConnected])
    }
    
    static func setAppleConnectedProperty(isConnected: Bool) {
        Mixpanel.mainInstance().people.set(properties: ["AppleMusic Connected": isConnected])
    }
    
    static func setiPodConnectedProperty(isConnected: Bool) {
        Mixpanel.mainInstance().people.set(properties: ["iPod Connected": isConnected])
    }

    static func setAlias(uid: String) {
        Mixpanel.mainInstance().createAlias(uid, distinctId: MXSender.getDistinctId())
        UserDefaults.standard.set(uid, forKey: "MixpanelAlias")
    }
    
    static func setPersonProperties(properties: Properties) {
        Mixpanel.mainInstance().people.set(properties: properties)
    }
    
    static func trackSpotifyConnected() {
        Mixpanel.mainInstance().track(event: "Music Connected", properties: ["Service": "Spotify"])
    }
    
    static func trackMaptrackEvent(id: String) {
        let savedFirstMaptrack = UserDefaults.standard.string(forKey: "SavedFirstMaptrack")
        if savedFirstMaptrack == nil {
            Mixpanel.mainInstance().track(event: "Saved First Maptrack", properties: ["MaptrackId": id])
            UserDefaults.standard.set(id, forKey: "SavedFirstMaptrack")
        }
    }
    
    static func trackLoginEvent() {
        Mixpanel.mainInstance().track(event: "User Login")
    }
    
    static func trackLogoutEvent() {
        Mixpanel.mainInstance().track(event: "User Logout")
    }
    
    static func trackCreateUserEvent() {
        Mixpanel.mainInstance().track(event: "New User Created")
    }
}

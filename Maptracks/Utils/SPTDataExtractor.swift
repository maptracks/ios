//
//  SPTDataExtractor.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/5/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct SPTDataExtractor {
    static func idFromResponse(data: [String: Any]) -> String {
        if let id = data["id"] as? String {
            return id
        }
        return ""
    }
    
    static func titleFromResponse(data: [String: Any]) -> String {
        if let title = data["name"] as? String {
            return title
        }
        return ""
    }
    
    static func artistFromResponse(data: [String: Any]) -> String {
        if let artists = data["artists"] as? [[String: Any]], let artist = artists.first {
            return artist["name"] as! String
        } else if let artist = data["artist"] as? [String: Any] {
            return artist["name"] as! String
        }
        return ""
    }
    
    static func albumFromResponse(data: [String: Any]) -> String {
        if let album = data["album"] as? [String: Any] {
            return album["name"] as! String
        }
        return ""
    }
    
    static func imageFromResponse(data: [String: Any]) -> String {
        if let album = data["album"] as? [String: Any] {
            if let images = album["images"] as? [[String: Any]], let image = images.last {
                return image["url"] as! String
            }
        }
        return ""
    }
    
    static func audioFromResponse(data: [String: Any]) -> String {
        if let uri = data["uri"] as? String {
            return uri
        }
        return ""
    }
    
    static func isrcFromResponse(data: [String: Any]) -> String {
        if let externals = data["external_ids"] as? [String: String] {
            if let isrc = externals["isrc"] {
                return isrc
            }
        }
        return ""
    }
    
    static func durationFromResponse(data: [String: Any]) -> Double {
        if let duration = data["duration_ms"] as? Double {
            return duration
        }
        return 0.0
    }
    
    static func trackFromResponseItem(json: [String: Any], trackId: String) -> Track {
        let title = titleFromResponse(data: json)
        let artist = artistFromResponse(data: json)
        let album = albumFromResponse(data: json)
        let audio = audioFromResponse(data: json)
        let image = imageFromResponse(data: json)
        let isrc = isrcFromResponse(data: json)
        let duration = durationFromResponse(data: json)
        let apple = ""
        let spotify = idFromResponse(data: json)
        return Track(id: trackId, maptrack: "", title: title, artist: artist, album: album, audio: audio, image: image, spotify: spotify, apple: apple, isrc: isrc, duration: duration)
    }
    
    static func tracksFromResponseItems(json: [String: Any]) -> [Track] {
        var tracks = [Track]()
        if let sptTracks = json["tracks"] as? [[String: Any]] {
            for data in sptTracks {
                let spotify = idFromResponse(data: data)
                let title = titleFromResponse(data: data)
                let artist = artistFromResponse(data: data)
                let album = albumFromResponse(data: data)
                let audio = audioFromResponse(data: data)
                let image = imageFromResponse(data: data)
                let isrc = isrcFromResponse(data: data)
                let duration = durationFromResponse(data: data)
                let apple = ""
                let trackId = MTFirestore.only.newTrackId()
                tracks.append(Track(id: trackId, maptrack: "", title: title, artist: artist, album: album, audio: audio, image: image, spotify: spotify, apple: apple, isrc: isrc, duration: duration))
            }
        }
        
        return tracks
    }
}

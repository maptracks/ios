//
//  LayoutUtils.swift
//  Maptracks
//
//  Created by Emily Kolar on 6/6/18.
//  Copyright © 2018 Emily Kolar. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

struct LayoutUtils {
    static func getTopInset() -> CGFloat {
        let insets = UIApplication.shared.keyWindow?.safeAreaInsets
        if let top = insets?.top {
            return top
        }
        return 0
    }
    
    static func getBottomInset() -> CGFloat {
        let insets = UIApplication.shared.keyWindow?.safeAreaInsets
        if let bottom = insets?.bottom {
            return bottom
        }
        return 0
    }
    
    static func getLeftInset() -> CGFloat {
        let insets = UIApplication.shared.keyWindow?.safeAreaInsets
        if let left = insets?.left {
            return left
        }
        return 0
    }
    
    static func getRightInset() -> CGFloat {
        let insets = UIApplication.shared.keyWindow?.safeAreaInsets
        if let right = insets?.right {
            return right
        }
        return 0
    }
}

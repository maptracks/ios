//
//  Location.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/6/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationData  {
    static func generatePathString(fromLocations locations: [CLLocationCoordinate2D]) -> String {
        var pathString = ""
        for i in 0..<locations.count {
            let loc = locations[i]
            pathString += String(loc.latitude)
            pathString += ","
            pathString += String(loc.longitude)
            if i < locations.count - 1 {
                pathString += "|"
            }
        }
        return pathString
    }
    
    static func generateMtGeos(fromSnappedPointsJson data: [[String: Any]], callback: @escaping ([MTGeo]?, Error?) -> Void) {
        let currentTimeString = Date().toString()
        let timestampedData = data.map({ (item: [String: Any]) -> [String: Any] in
            return [
                "location": item["location"]!,
                "placeId": item["placeId"]!,
                "created": currentTimeString
            ]
        })
            
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: timestampedData, options: .prettyPrinted)
            
            let decoder = JSONDecoder()
            let geopoints = try decoder.decode([MTGeo].self, from: jsonData)
 
            callback(geopoints, nil)
        } catch let err {
            callback(nil, err)
        }
    }
}




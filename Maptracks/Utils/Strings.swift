//
//  Strings.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/18/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct StringUtils {
    static func randomString(length: Int) -> String {
        let alphas = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
        var text = ""
        for _ in 0..<length {
            let index = Int(arc4random_uniform(UInt32(alphas.count)))
            let charIndex = alphas.index(alphas.startIndex, offsetBy: index)
            let char = alphas[charIndex]
            text.append(char)
        }
        return text
    }
    
    static func makeAlbumImageFilename(album: String, artist: String) -> String {
        let albumLower = album.lowercased().replacingOccurrences(of: " ", with: "-")
        let artistLower = artist.lowercased().replacingOccurrences(of: " ", with: "-")
        let joined = artistLower.appending("_").appending(albumLower)
        print(joined)
        return joined
    }
    
    static func stringByAddingPercentEncoding(originalString: String) -> String {
        let unreserved = "-_"
        var allowed = CharacterSet.urlFragmentAllowed
        allowed.insert(charactersIn: unreserved)
        return originalString.addingPercentEncoding(withAllowedCharacters: allowed) ?? ""
    }
}

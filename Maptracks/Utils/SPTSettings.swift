//
//  SPTSettings.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct SPTSettings {
    static let CLIENT_ID = "a568fd7163b44e5881e1d4ebcaeacd85"
    static let REDIRECT_URL = "maptracks://spotify-login"
    static let SESSION_DEFAULTS_KEY = "current session"
    static let SESSION_OBJECT_KEY = "SpotifySession"
    static let TOKEN_SWAP_URL = "https://maptracks.me/api/swap"
    static let TOKEN_REFRESH_URL = "https://maptracks.me/api/refresh"
    
    static let FIRST_LOGIN_NOTIF_NAME = "spotifyLoginSuccess"

    static let API_BASE = "https://api.spotify.com/v1"
    static let CURRENTLY_PLAYING = "/me/player/currently-playing"
    static let TRACKS = "/tracks"
    static let USERS_UID_PLAYLISTS = "/playlists"
}

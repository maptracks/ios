//
//  GApiSettings.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/7/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation


struct GApiSettings {
    static let FIRESTORAGE_MAPTRACKS = "maptracks/"
    static let FIRESTORASGE_TRACKS = "tracks/"
    static let API_KEY = "AIzaSyDfoI519lCqWBLn2Lj4uHn3TFZqHvME1bA"
    static let API_BASE = "https://roads.googleapis.com/v1"
    static let SNAP_ROADS = "/snapToRoads"
    static let DEFAULT_TRACK_IMAGE = "https://firebasestorage.googleapis.com/v0/b/maptracks-66900.appspot.com/o/tracks%2Fdefault.png?alt=media&token=73cc444f-f68f-4532-9341-b52937a84e00"
}

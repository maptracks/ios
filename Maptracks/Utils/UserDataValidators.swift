//
//  UserDataValidators.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct UserDataValidators {
    static func passwordResult(password: String) -> Bool {
        return password.count >= 8
    }
}

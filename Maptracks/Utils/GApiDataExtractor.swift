//
//  GApiDataExtractor.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/7/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct GApiDataExtractor {
    static func generateWaypoints(fromSnappedData json: [[String: Any]]) -> [Waypoint] {
        let parsed = json.map({ (item: [String: Any]) -> [String: Double] in
            let loc = item["location"] as! [String: Double]
            let lat = loc["latitude"]!
            let long = loc["longitude"]!
            return [
                "lat": lat,
                "long": long
            ]
        })
        do {
            let data = try JSONSerialization.data(withJSONObject: parsed, options: .prettyPrinted)
            let decoder = JSONDecoder()
            let waypoints = try decoder.decode([Waypoint].self, from: data)
            return waypoints
        } catch let err {
            print(err.localizedDescription)
        }
        return [Waypoint]()
    }
}

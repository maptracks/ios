//
//  GMSettings.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct GMSettings {
    static let SERVICES_API_KEY = "AIzaSyABzp8mXoC8COxL89HTSarwH94yus6h0Xw"
    static let BUCKETS: [NSNumber] = [5, 10, 20, 30, 50, 100]
    static let CLUSTER_COLORS = [MTColor.green, MTColor.green, MTColor.pink, MTColor.pink, MTColor.blue, MTColor.purple]
    static let POLY_COLORS = [MTColor.green, MTColor.pink, MTColor.blue, MTColor.purple]
    static let POLY_WIDTH: CGFloat = 5.0
    static let MAP_ZOOM: Float = 14.0
    
    static let DEFAULT_LAT: Double = -33.868
    static let DEFAULT_LNG: Double = 151.2086
}

//
//  Settings.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct Settings {
    static let BUTTON_SIZE: CGFloat = 40.0
    static let BUTTON_Y: CGFloat = 12.0
    static let BUTTON_RADIUS: CGFloat = 20.0
    static let BUTTON_SHADOW_OPACITY: Float = 0.3
    static let BUTTON_SHADOW_RADIUS: CGFloat = 5.0
    static let BUTTON_SHADOW_OFFSET = CGSize(width: 0, height: 5.0)
    
    static let SNACKBAR_HEIGHT: CGFloat = 80
    
    static let EDIT_PROMPT_TEXT = "Write anything here! Some ideas to get you started: Why this song made your trip awesome, a cool travel tip, your mom's snickerdoodle recipe, perhaps a haiku..."
}

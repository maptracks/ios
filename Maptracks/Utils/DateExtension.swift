//
//  DateExtension.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/5/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

extension Date {
    func toString() -> String {
        return String(format:"%f", self.timeIntervalSince1970)
    }
}

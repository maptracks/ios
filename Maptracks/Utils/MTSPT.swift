//
//  SPTExtension.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/18/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct MTSPT {
    static let MTSPTAuthCurrentlyPlayingScope: NSString = "user-read-currently-playing"
    static let MTSPTAuthRecentlyPlayedScope: NSString = "user-read-recently-played"
}

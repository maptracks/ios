//
//  ConfigSettings.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/30/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct ConfigSettings {
    static let kAppleMusicTokenKey = "appleMusicToken"
}

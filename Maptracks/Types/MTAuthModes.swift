//
//  AuthModes.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

enum AuthMode: Int {
    case login = 0, signup
}

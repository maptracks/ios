//
//  MTStoreQuery.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

enum MTStoreQuery: Int {
    case nearby = 0, history
}

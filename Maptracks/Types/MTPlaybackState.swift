//
//  MTPlaybackState.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/6/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

enum MTPlaybackState: Int {
    case playing = 0, paused, stopped
}

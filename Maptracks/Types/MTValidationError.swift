//
//  ValidationError.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct ValidationError: Error {
    public let message: String
    
    public init(_ msg: String) {
        message = msg
    }
}

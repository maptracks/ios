//
//  MTColor.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import UIKit

struct MTColor {
    static let green = UIColor(red: 0, green: 0.76, blue: 0.67, alpha: 1.0)
    static let pink = UIColor(red: 0.83, green: 0.2, blue: 0.3, alpha: 1.0)
    static let purple = UIColor(red: 0.41, green: 0.27, blue: 0.82, alpha: 1.0)
    static let blue = UIColor(red: 0.1, green: 0.58, blue: 0.92, alpha: 1.0)
    static let background = UIColor(red: 0.05, green: 0.09, blue: 0.18, alpha: 1.0)
    static let salmon = UIColor(red: 1.0, green: 0.4, blue: 0.4, alpha: 1.0)
    static let backgroundClear = UIColor(red: 0.05, green: 0.09, blue: 0.18, alpha: 0.0)

    static let inputBorder = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2)
    static let inputPlaceholder = UIColor(red: 1, green: 1, blue: 1, alpha: 0.6)
    
    // lower alpha values
    static let greenSheer = UIColor(red: 0, green: 0.76, blue: 0.67, alpha: 0.4)
    static let pinkSheer = UIColor(red: 0.83, green: 0.2, blue: 0.3, alpha: 0.4)
    static let purpleSheer = UIColor(red: 0.41, green: 0.27, blue: 0.82, alpha: 0.4)
    static let blueSheer = UIColor(red: 0.1, green: 0.58, blue: 0.92, alpha: 0.4)
    static let salmonSheer = UIColor(red: 1.0, green: 0.4, blue: 0.4, alpha: 0.4)
}


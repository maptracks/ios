//
//  Route.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/9/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import CoreLocation
import FirebaseFirestore

struct Route: Codable {
    var id: String
    var maptrack: String
    var waypoints: [Waypoint]
    
    var dictionary: [String: Any] {
        return [
            "id": id,
            "maptrack": maptrack,
            "waypoints": waypoints,
        ]
    }
}

extension Route: DocumentSerializable {
    init?(dictionary: [String: Any]) {
        guard let points = dictionary["waypoints"] as? [GeoPoint],
            let id = dictionary["id"] as? String,
            let maptrack = dictionary["maptrack"] as? String
            else { return nil }
        
        var waypoints = [Waypoint]()
        for pt in points {
            let newPt = Waypoint.fromGeoPoint(geopoint: pt)
            waypoints.append(newPt)
        }

        self.init(id: id, maptrack: maptrack, waypoints: waypoints)
    }
}


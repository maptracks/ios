//
//  Waypoint.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/9/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Waypoint: Codable {
    var lat: Double
    var long: Double
    
    var dictionary: [String: Double] {
        return [
            "lat": lat,
            "long": long
        ]
    }
}

extension Waypoint: DocumentSerializable {
    init?(dictionary: [String: Any]) {
        guard let lat = dictionary["lat"] as? Double,
            let long = dictionary["long"] as? Double
            else { return nil }
        
        self.init(lat: lat, long: long)
    }
}

extension Waypoint {
    var toGeoPoint: GeoPoint {
        get {
            return GeoPoint(latitude: lat, longitude: long)
        }
    }
    
    static func fromGeoPoint(geopoint: GeoPoint) -> Waypoint {
        return Waypoint(lat: geopoint.latitude, long: geopoint.longitude)
    }
}




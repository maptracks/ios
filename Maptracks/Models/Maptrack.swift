//
//  Maptrack.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/9/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Maptrack {
    var id: String
    var comment: String
    var image: String
    var start: Double
    var end: Double
    var track: String
    var user: String
    var route: String
    var midpoint: Waypoint
    var minpoint: Waypoint
    var maxpoint: Waypoint
    
    var dictionary: [String: Any] {
        return [
            "id": id,
            "comment": comment,
            "image": image,
            "track": track,
            "start": start,
            "end": end,
            "user": user,
            "route": route,
            "midpoint": midpoint,
            "minpoint": minpoint,
            "maxpoint": maxpoint
        ]
    }
}

extension Maptrack: DocumentSerializable {
    init?(dictionary: [String: Any]) {
        guard let comment = dictionary["comment"] as? String,
            let image = dictionary["image"] as? String,
            let start = dictionary["start"] as? Double,
            let end = dictionary["end"] as? Double,
            let track = dictionary["track"] as? String,
            let user = dictionary["user"] as? String,
            let route = dictionary["route"] as? String,
            let midpoint = dictionary["midpoint"] as? GeoPoint,
            let minpoint = dictionary["minpoint"] as? GeoPoint,
            let maxpoint = dictionary["maxpoint"] as? GeoPoint
            else { return nil }
        
        let mid = Waypoint.fromGeoPoint(geopoint: midpoint)
        let min = Waypoint.fromGeoPoint(geopoint: minpoint)
        let max = Waypoint.fromGeoPoint(geopoint: maxpoint)

        var maptrackId = ""
        if let id = dictionary["id"] as? String {
            maptrackId = id
        }
        
        self.init(id: maptrackId, comment: comment, image: image, start: start, end: end, track: track, user: user, route: route, midpoint: mid, minpoint: min, maxpoint: max)
    }
}

//
//  MTUser.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

struct MTUser {
    var fullName: String
    var uid: String
    var email: String
    var photoUrl: String
    var created: Date
    var updated: Date
    var maptracks: [String]
    
    var dictionary: [String: Any] {
        return [
            "fullName": fullName,
            "uid": uid,
            "email": email,
            "photoUrl": photoUrl,
            "created": created,
            "updated": updated,
            "maptracks": maptracks
        ]
    }
}

extension MTUser: DocumentSerializable {
    init?(dictionary: [String: Any]) {
        guard let fullName = dictionary["fullName"] as? String,
            let uid = dictionary["uid"] as? String,
            let email = dictionary["email"] as? String,
            let photoUrl = dictionary["photoUrl"] as? String,
            let created = dictionary["created"] as? Date,
            let updated = dictionary["updated"] as? Date,
            let maptracks = dictionary["maptracks"] as? [String]
            else { return nil }
        
        self.init(
            fullName: fullName,
            uid: uid,
            email: email,
            photoUrl: photoUrl,
            created: created,
            updated: updated,
            maptracks: maptracks
        )
    }
}



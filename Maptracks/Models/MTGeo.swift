//
//  MTGeo.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import CoreLocation
import FirebaseFirestore

struct MTGeo: Codable {
    var location: [String: Double]
    var placeId: String
    var created: String
    
    var dictionary: [String: Any] {
        return [
            "location": location,
            "placeId": placeId,
            "created": created
        ]
    }
}

extension MTGeo: DocumentSerializable {
    init?(dictionary: [String: Any]) {
        guard let location = dictionary["location"] as? [String: Double],
            let placeId = dictionary["placeId"] as? String
            else { return nil }
        
        var created = ""
        
        if let time = dictionary["created"] as? String {
            created = time
        } else {
            created = Date().toString()
        }
        
        self.init(location: location, placeId: placeId, created: created)
    }
}



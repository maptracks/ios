//
//  Track.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/9/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

class CurrentTrack {
    var track: Track
    var isPlaying: Bool
    var progress: Double
    var timestamp: Double
    
    init(track: Track, isPlaying: Bool, progress: Double, timestamp: Double) {
        self.track = track
        self.isPlaying = isPlaying
        self.progress = progress
        self.timestamp = timestamp
    }
}

struct Track: Codable {
    var id: String
    var maptrack: String
    var title: String
    var artist: String
    var album: String
    var audio: String
    var image: String
    var spotify: String
    var apple: String
    var isrc: String
    var duration: Double
    
    var dictionary: [String: Any] {
        return [
            "id": id,
            "maptrack": maptrack,
            "title": title,
            "artist": artist,
            "album": album,
            "audio": audio,
            "image": image,
            "apple": apple,
            "spotify": spotify,
            "isrc": isrc,
            "duration": duration
        ]
    }
}

extension Track: DocumentSerializable {
    init?(dictionary: [String: Any]) {
        guard let id = dictionary["id"] as? String,
            let maptrack = dictionary["maptrack"] as? String,
            let title = dictionary["title"] as? String,
            let artist = dictionary["artist"] as? String,
            let album = dictionary["album"] as? String,
            let audio = dictionary["audio"] as? String,
            let image = dictionary["image"] as? String,
            let spotify = dictionary["spotify"] as? String,
            let apple = dictionary["apple"] as? String,
            let isrc = dictionary["isrc"] as? String,
            let duration = dictionary["duration"] as? Double
            else { return nil }
        
        self.init(id: id, maptrack: maptrack, title: title, artist: artist, album: album, audio: audio, image: image, spotify: spotify, apple: apple, isrc: isrc, duration: duration)
    }
}

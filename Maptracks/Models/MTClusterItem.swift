//
//  MTClusterItem.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation

class MTClusterItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var maptrack: Maptrack!
    
    init(position: CLLocationCoordinate2D, maptrack: Maptrack) {
        self.position = position
        self.maptrack = maptrack
    }
}

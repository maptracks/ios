//
//  HistoryTableViewController.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/10/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class HistoryTableViewController: UITableViewController, UINavigationControllerDelegate {
    var maptracks: [Maptrack]!
    var trackInfo: [String: Track]!
    
    var backButton: BackButton!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        trackInfo = [String: Track]()
        prepareData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupBackButton() {
        let width = UIScreen.main.bounds.size.width
        backButton = BackButton.instanceFromNib(x: width / 7, y: nil)
        backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        view.addSubview(backButton)
        view.bringSubview(toFront: backButton)
    }
    
    func prepareData() {
        var count = 0
        DispatchQueue.global(qos: .userInteractive).async {
            for item in self.maptracks {
                MTFirestore.only.getTrack(forTrackId: item.track, callback: { track, error in
                    count += 1
                    if let err = error { print(err.localizedDescription) }
                    if let t = track {
                        DispatchQueue.main.async {
                            self.trackInfo[item.id] = t
                            print(self.trackInfo)
                            if count >= self.maptracks.count {
                                self.tableView.reloadData()
                            }
                        }
                    }
                })
            }
        }
    }
    
    @objc func goBack() {
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return maptracks.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Mapped Tracks"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let nib = Bundle.main.loadNibNamed("HistoryTableViewCell", owner: self, options: nil)

        let cell = nib?.first as? HistoryTableViewCell

        cell!.tag = indexPath.row
        cell?.editButton.tag = indexPath.row
        
        let maptrack = maptracks[indexPath.row]
        
        var url: URL!
        
        cell!.albumCover.image = nil
        cell!.maptrack = maptrack
        
        if let track = trackInfo[maptrack.id] {
            cell!.titleLabel.text = track.title
            cell!.artistLabel.text = track.artist
            cell!.editButton.maptrack = maptrack
            cell!.editButton.track = track
            cell!.editButton.addTarget(self, action: #selector(editAction), for: .touchUpInside)
            
            url = URL(string: track.image)
            
            DispatchQueue.global(qos: .userInteractive).async {
                MTDownloader.downloadImage(url: url!, callback: { image, error in
                    if let err = error {
                        print(err.localizedDescription)
                    } else if let img = image {
                        DispatchQueue.main.async {
                            if cell!.tag == indexPath.row {
                                cell!.albumCover.image = img
                            }
                        }
                    }
                })
            }
        }

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = Bundle.main.loadNibNamed("TableHeader", owner: self, options: nil)
        let header = nib?.first as? TableHeader
        header?.backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        return header!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? HistoryTableViewCell {
            guard let maptrack = cell.editButton.maptrack,
                let track = cell.editButton.track else { return }
            showEditViewController(forMaptrack: maptrack, forTrack: track)
//            self.launchEditView(withMaptrack: maptrack, withTrack: track)
        }
    }
    
    
    @objc func editAction(_ button: EditHistoryButton) {
        if let maptrack = button.maptrack {
            showEditViewController(forMaptrack: maptrack, forTrack: button.track)
//            self.launchEditView(withMaptrack: maptrack, withTrack: button.track)
        }
    }
    
    func showEditViewController(forMaptrack maptrack: Maptrack, forTrack track: Track) {
        let editVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "editMaptrack") as! EditMaptrackViewController
        editVC.maptrack = maptrack
        editVC.track = track
        present(editVC, animated: true, completion: nil)
    }
    
//    func launchEditView(withMaptrack: Maptrack, withTrack: Track) {
//        editView = EditMaptrackView.instanceFromNib(frame: view.frame)
//        editView?.maptrack = withMaptrack
//        editView?.saveButton.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
//        editView?.backButton.addTarget(self, action: #selector(closeEditAction), for: .touchUpInside)
//        editView?.uploadImageButton.addTarget(self, action: #selector(chooseImageAction), for: .touchUpInside)
//        editView?.titleLabel.text = withTrack.title
//        editView?.artistLabel.text = withTrack.artist
//        MTDownloader.downloadImage(url: URL(string: withTrack.image)!, callback: { image, error in
//            self.editView?.albumCover.image = image
//        })
//        view.addSubview(editView!)
//        editView?.slideInLeft(callback: { _ in
//            self.tableView.isScrollEnabled = false
//        })
//    }
    
//    @objc func closeEditAction() {
//        editView?.slideOutRight {
//            self.editView = nil
//            self.tableView.isScrollEnabled = true
//        }
//    }
//    
//    @objc func saveAction(_ button: UIButton) {
//        print(editView?.commentView.text)
//        print(editView?.maptrack)
//    }
//    
//    @objc func chooseImageAction(_ button: UIButton) {
//        imagePicker.allowsEditing = false
//        imagePicker.sourceType = .photoLibrary
//        present(imagePicker, animated: true, completion: nil)
//    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension HistoryTableViewController: UIImagePickerControllerDelegate {
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            editView?.uploadImageButton.imageView?.contentMode = .scaleAspectFit
//            editView?.uploadImageButton.imageView?.image = pickedImage
//        }
//        picker.dismiss(animated: true, completion: nil)
//    }
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        picker.dismiss(animated: true, completion: nil)
//    }
//}



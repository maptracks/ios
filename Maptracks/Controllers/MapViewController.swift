//
//  MapViewController.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import CoreLocation
import GoogleMaps
import GooglePlaces
import Alamofire

class MapViewController: UIViewController {
    // MARK: - Map Properties

    // manage clustered map markers
    var clusterManager: GMUClusterManager!
    
    // elements to display on the map
    var mapView: GMSMapView?
    var marker: GMSMarker?
    var routeLine: GMSPolyline!
    var polys = [GMSPolyline]()
    var nearbyMarkers = [MTClusterItem]()
    var historicalMarkers = [MTClusterItem]()
    
    // toggle for updating the marker (every other)
    var markerUpdated = false

    // track selected from history/explore, displays in audio player snackbar
    var displayTrack: Track?
    // route selected from history/explore
    var displayLine: GMSPolyline!
    // tracks to display in table list when marker(s) tapped
    var selectedTracks = [String]()
    
    // cycle thru these to draw the route lines
    var routeColors: [UIColor] = [MTColor.green, MTColor.pink, MTColor.blue, MTColor.purple]
    
    // MARK: - Button Controls and Subviews
    
    var backButton: BackButton!
    var centerMapButton: CenterMapButton!
    var historyButton: HistoryButton!
    var nearbyButton: NearbyButton!
    var snackbar: AudioSnackbarView?
    
    // MARK: - Overrides and Lifecycle
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNotifications()

        setupMap()
        
        if MTLocManager.only.delegate != nil {
            MTLocManager.only.delegate = nil
        }

        MTLocManager.only.delegate = self
        
        setupClustering()
        
        setupSubViews()
    }
    
    // listen for currently playing notifications
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleSpotifyPlayingNotification), name: Notification.Name(rawValue: "spotifyCurrentlyPlaying"), object: MTSpotify.only)
        NotificationCenter.default.addObserver(self, selector: #selector(handleMusicAppPlayingNotification), name: Notification.Name(rawValue: "musicAppCurrentlyPlaying"), object: MTMusicApp.only)
    }
    
    @objc func handleSpotifyPlayingNotification(_ notification: Notification) {
        guard let data = notification.userInfo as? [String: Any],
            let track = data["track"] as? Track,
            let progress = data["progress"] as? Double,
            let isPlaying = data["isPlaying"] as? Bool,
            let timestamp = data["timestamp"] as? Double else { return }
        if isPlaying {
            self.handleSpotifyCurrentlyPlaying(track: track, progress: progress, timestamp: timestamp)
        }
    }

    @objc func handleMusicAppPlayingNotification(_ notification: Notification) {
        print("map notification")
        guard let data = notification.userInfo as? [String: Any],
            let track = data["track"] as? Track,
            let progress = data["progress"] as? Double,
            let isPlaying = data["isPlaying"] as? Bool,
            let timestamp = data["timestamp"] as? Double else { return }
        if isPlaying {
            self.handleMusicAppCurrentlyPlaying(track: track, progress: progress, timestamp: timestamp)
        }
    }
    
    // remove currently playing notifications
    func teardownNotifications() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "spotifyCurrentlyPlaying"), object: MTSpotify.only)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "musicAppCurrentlyPlaying"), object: MTMusicApp.only)
    }
    
    // create the google map object + view
    func setupMap() {
        var camera: GMSCameraPosition!
        
        if let start = MTSessionData.only.initialLocation {
            camera = GMSCameraPosition.camera(withLatitude: start.latitude, longitude: start.longitude, zoom: GMSettings.MAP_ZOOM)
        } else {
            camera = GMSCameraPosition.camera(withLatitude: GMSettings.DEFAULT_LAT, longitude: GMSettings.DEFAULT_LNG, zoom: GMSettings.MAP_ZOOM)
        }
        
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView?.delegate = self
        
        marker = GMSMarker(position: camera.target)
        
        // position the marker's ground anchor to center center
        marker?.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker?.icon = UIImage(named: "smallMarker")!
        marker?.map = mapView
        markerUpdated = true
        
        view.addSubview(mapView!)
    }

    // init the map controls and subviews
    func setupSubViews() {
        let width = UIScreen.main.bounds.size.width
        backButton = BackButton.instanceFromNib(x: width / 7, y: nil)
        centerMapButton = CenterMapButton.instanceFromNib(x: (width * 6) / 7)
        historyButton = HistoryButton.instanceFromNib(x: (width * 2) / 5)
        nearbyButton = NearbyButton.instanceFromNib(x: (width * 3) / 5)
        
        backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        centerMapButton.addTarget(self, action: #selector(goCenter), for: .touchUpInside)
        nearbyButton.addTarget(self, action: #selector(goNearby), for: .touchUpInside)
        historyButton.addTarget(self, action: #selector(goHistory), for: .touchUpInside)
        
        view.addSubview(backButton)
        view.bringSubview(toFront: backButton)
        view.addSubview(centerMapButton)
        view.bringSubview(toFront: centerMapButton)
        view.addSubview(nearbyButton)
        view.bringSubview(toFront: nearbyButton)
        view.addSubview(historyButton)
        view.bringSubview(toFront: historyButton)
    }

    // initialize marker clustering
    func setupClustering() {
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: GMSettings.BUCKETS, backgroundColors: GMSettings.CLUSTER_COLORS)
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView!, clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView!, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
    }
    
    // go back to the dashboard
    @objc func goBack() {
        teardownNotifications()
        dismiss(animated: true, completion: nil)
    }
    
    // center the map to current-location marker
    @objc func goCenter() {
        centerMapCamera()
    }
    
    // see nearby maptracks
    @objc func goNearby() {
        nearbyButton.toggleOn()
        clearNearbyClusters()
        if nearbyButton.isOn {
            queryForMaptracks()
        } else {
            removeSnackbar() {
                return
            }
        }
    }
    
    // see your maptracks history
    @objc func goHistory() {
        historyButton.toggleOn()
        clearHistoryClusters()
        if historyButton.isOn {
            queryForHistory()
        } else {
            removeSnackbar() {
                return
            }
        }
    }

    // draw the polyline for previous session route
    func drawLastRoute() {
        if MTSessionData.only.routes.count == 0 { return }

        DispatchQueue.global().async {
            let colorIndex = MTSessionData.only.routes.count - 1 >= self.routeColors.count ? MTSessionData.only.routes.count - 1 % self.routeColors.count : MTSessionData.only.routes.count - 1
            let color = self.routeColors[colorIndex >= 0 ? colorIndex : 0]
            let path = GMSMutablePath()

            for pt in MTSessionData.only.lastWaypoints {
                path.add(CLLocationCoordinate2D(latitude: pt.lat, longitude: pt.long))
            }

            DispatchQueue.main.async {
                let line = GMSPolyline(path: path)
                line.strokeColor = color
                line.strokeWidth = GMSettings.POLY_WIDTH
                self.polys.append(line)
                line.map = self.mapView
            }
        }
    }
    
    // update the polyline for active session route
    func drawRoute() {
        if MTSessionData.only.activeRoute == nil { return }

        DispatchQueue.global().async {
            let colorIndex = MTSessionData.only.routes.count >= self.routeColors.count ? MTSessionData.only.routes.count % self.routeColors.count : MTSessionData.only.routes.count
            let color = self.routeColors[colorIndex]
            let path = GMSMutablePath()

            for pt in MTSessionData.only.activeWaypoints {
                path.add(CLLocationCoordinate2D(latitude: pt.lat, longitude: pt.long))
            }
            
            DispatchQueue.main.async {
                if let line = self.routeLine {
                    line.map = nil
                }

                self.routeLine = GMSPolyline(path: path)
                self.routeLine.strokeColor = color
                self.routeLine.strokeWidth = GMSettings.POLY_WIDTH
                self.routeLine.map = self.mapView
            }
        }
    }
    
    // draw a route from history or other users
    func drawRetrievedRoute(route: Route) {
        DispatchQueue.global().async {
            let path = GMSMutablePath()
            
            for pt in (route.waypoints) {
                path.add(CLLocationCoordinate2D(latitude: pt.lat, longitude: pt.long))
            }
            
            DispatchQueue.main.async {
                if let line = self.displayLine {
                    line.map = nil
                }
                self.displayLine = GMSPolyline(path: path)
                self.displayLine.strokeColor = MTColor.salmon
                self.displayLine.strokeWidth = GMSettings.POLY_WIDTH
                self.displayLine.map = self.mapView
            }
        }
    }

    // render the location marker at last received coordinates
    func repositionMapMarker(toLocation location: CLLocationCoordinate2D) {
        marker?.position = location
        markerUpdated = true
    }
    
    // animate the map camera to marker location
    func centerMapCamera() {
        let zoom = mapView?.camera.zoom
        let markerLocation = marker?.position
        let update = GMSCameraUpdate.setTarget(markerLocation!, zoom: zoom!)
        mapView?.animate(with: update)
    }
    
    // handle after retrieving a currently playing song from spotify
    // NOTE: called from a separate thread, return to main thread is in method
    func handleSpotifyCurrentlyPlaying(track: Track, progress: Double, timestamp: Double) {
        if MTSessionData.only.activeTrack == nil {
            MTSessionData.only.activeTrack = track
            MTSessionData.only.trackStart = timestamp - progress
            MTSessionData.only.trackProgress = progress
        } else if MTSessionData.only.activeTrack?.spotify == track.spotify && progress >= MTSessionData.only.trackProgress! {
            MTSessionData.only.trackProgress = progress
        } else {
            print("map saving spotify track")
            MTSessionData.only.tracks.append(MTSessionData.only.activeTrack!)
            
            if let route = MTSessionData.only.activeRoute {
                var maxLat: Double = -180
                var maxLong: Double = -180
                var minLat: Double = 180
                var minLong: Double = 180
                
                for pt in route.waypoints {
                    if pt.lat > maxLat {
                        maxLat = pt.lat
                    }
                    if pt.long > maxLong {
                        maxLong = pt.long
                    }
                    if pt.lat < minLat {
                        minLat = pt.lat
                    }
                    if pt.long < minLong {
                        minLong = pt.long
                    }
                }
                
                let max = Waypoint(lat: maxLat, long: maxLong)
                let min = Waypoint(lat: minLat, long: minLong)
                
                let midLat: Double = max.lat - ((max.lat - min.lat) / 2)
                let midLong: Double = max.long - ((max.long - min.long) / 2)
                let mid = Waypoint(lat: midLat, long: midLong)
                
                let maptrackId = MTFirestore.only.newMaptrackId()
                let routeId = MTSessionData.only.activeRoute!.id
                let trackId = MTSessionData.only.activeTrack!.id
                
                let newMaptrack = Maptrack(id: maptrackId, comment: "", image: "", start: MTSessionData.only.routeStart, end: timestamp, track: trackId, user: MTSessionData.only.uid, route: routeId, midpoint: mid, minpoint: min, maxpoint: max)

                MTFirestore.only.save(route: route, id: routeId, maptrackId: maptrackId)
                MTFirestore.only.save(track: MTSessionData.only.activeTrack!, id: trackId, maptrackId: maptrackId)
                
                MTFirestore.only.save(maptrack: newMaptrack, id: maptrackId)
                
                MTSessionData.only.routes.append(route)
            }
            
            MTSessionData.only.activeTrack = track
            MTSessionData.only.activeRoute = nil
            MTSessionData.only.waypoints.removeAll()
            MTSessionData.only.trackProgress = progress
            MTSessionData.only.trackStart = timestamp - progress
            
            DispatchQueue.main.async {
                self.drawLastRoute()
            }
        }
    }
    
    // handle after retrieving a currently playing song from spotify
    // NOTE: called from a separate thread, return to main thread is in method
    func handleMusicAppCurrentlyPlaying(track: Track, progress: Double, timestamp: Double) {
        if MTSessionData.only.activeTrack == nil {
            MTSessionData.only.activeTrack = track
            MTSessionData.only.trackStart = timestamp - progress
            MTSessionData.only.trackProgress = progress
        } else if MTSessionData.only.activeTrack?.apple == track.apple && progress >= MTSessionData.only.trackProgress! {
            MTSessionData.only.trackProgress = progress
        } else {
            print("map saving music app track")
            MTSessionData.only.tracks.append(MTSessionData.only.activeTrack!)
            
            if let route = MTSessionData.only.activeRoute {
                var maxLat: Double = -180
                var maxLong: Double = -180
                var minLat: Double = 180
                var minLong: Double = 180
                
                for pt in route.waypoints {
                    if pt.lat > maxLat {
                        maxLat = pt.lat
                    }
                    if pt.long > maxLong {
                        maxLong = pt.long
                    }
                    if pt.lat < minLat {
                        minLat = pt.lat
                    }
                    if pt.long < minLong {
                        minLong = pt.long
                    }
                }
                
                let max = Waypoint(lat: maxLat, long: maxLong)
                let min = Waypoint(lat: minLat, long: minLong)
                
                let midLat: Double = max.lat - ((max.lat - min.lat) / 2)
                let midLong: Double = max.long - ((max.long - min.long) / 2)
                let mid = Waypoint(lat: midLat, long: midLong)
                
                let maptrackId = MTFirestore.only.newMaptrackId()
                let routeId = MTSessionData.only.activeRoute!.id
                let trackId = MTSessionData.only.activeTrack!.id
                
                let newMaptrack = Maptrack(id: maptrackId, comment: "", image: "", start: MTSessionData.only.routeStart, end: timestamp, track: trackId, user: MTSessionData.only.uid, route: routeId, midpoint: mid, minpoint: min, maxpoint: max)
                
                MTFirestore.only.save(route: route, id: routeId, maptrackId: maptrackId)
                MTFirestore.only.save(track: MTSessionData.only.activeTrack!, id: trackId, maptrackId: maptrackId)
                
                MTFirestore.only.save(maptrack: newMaptrack, id: maptrackId)
                
                MTSessionData.only.routes.append(route)
            }
            
            MTSessionData.only.activeTrack = track
            MTSessionData.only.activeRoute = nil
            MTSessionData.only.waypoints.removeAll()
            MTSessionData.only.trackProgress = progress
            MTSessionData.only.trackStart = timestamp - progress
            
            DispatchQueue.main.async {
                self.drawLastRoute()
            }
        }
    }
    
    // handle retrieving snap-to-roads locations
    // NOTE: called from a separate thread, return to main thread is in method
    func handleSnappedPoints(fromResponse snappedData: [[String: Any]]) {
        let waypoints = GApiDataExtractor.generateWaypoints(fromSnappedData: snappedData)
        
        let now = Date().timeIntervalSince1970
        
        if MTSessionData.only.routeStart == 0 {
            MTSessionData.only.routeStart = now
        }
        
        MTSessionData.only.waypoints.append(contentsOf: waypoints)
        
        if MTSessionData.only.activeRoute == nil {
            let routeId = MTFirestore.only.newRouteId()
            MTSessionData.only.activeRoute = Route(id: routeId, maptrack: "", waypoints: MTSessionData.only.waypoints)
        } else {
            MTSessionData.only.activeRoute?.waypoints.append(contentsOf: waypoints)
            MTSessionData.only.routeEnd = now
        }
        
        DispatchQueue.main.async {
            self.drawRoute()
        }
    }
    
    // handle when a user requests to view a track
    // check if the snackbar is already active, dismiss if so
    func handleTrackView(track: Track) {
        displayTrack = track
        
        if snackbar != nil {
            removeSnackbar() {
                self.displaySnackbar(track: track)
            }
        } else {
            displaySnackbar(track: track)
        }
    }
    
    func handleTrackRouteView(track: Track) {
        DispatchQueue.global(qos: .userInitiated).async {
            MTFirestore.only.getMaptrack(forMaptrackId: track.maptrack, callback: { maptrack, error in
                if let err = error {
                    print(err.localizedDescription)
                } else if let mt = maptrack {
                    MTFirestore.only.getRoute(forRouteId: mt.route, callback: { route, error in
                        if let err = error {
                            print(err.localizedDescription)
                        }
                        if let r = route {
                            DispatchQueue.main.async {
                                self.drawRetrievedRoute(route: r)
                            }
                        }
                    })
                }
            })
        }
    }
    
    func removeDisplayRouteLine() {
        if let line = displayLine {
            line.map = nil
        }
    }
    
    func removeSnackbar(callback: @escaping () -> Void) {
        snackbar?.slideDownThen(callback: {
            self.snackbar = nil
            callback()
        })
    }
    
    // show the snackbar with track info
    func displaySnackbar(track: Track) {
        snackbar = AudioSnackbarView.instanceFromNib(y: view.frame.height - Settings.SNACKBAR_HEIGHT, width: view.frame.width)
        snackbar?.track = track
        snackbar?.setupLabels()
        view.addSubview(snackbar!)
        
        let playBtnX = (snackbar?.frame.width)! - Settings.BUTTON_SIZE - 5
        snackbar?.playbackButton = PlaybackButton.instanceFromNib(x: playBtnX, y: 20)
        snackbar?.playbackButton.setClear()
        snackbar?.playbackButton.addTarget(self, action: #selector(handleSnackbarPlayButtonAction(_:)), for: .touchUpInside)
        
        snackbar?.playbackButton.track = track
        snackbar?.addSubview((snackbar?.playbackButton)!)
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
        swipe.direction = UISwipeGestureRecognizerDirection.down
        
        let albumTap = UITapGestureRecognizer(target: self, action: #selector(snackbarAlbumImageAction))
        snackbar?.albumCover.addGestureRecognizer(albumTap)
        
        snackbar?.swipeView.addGestureRecognizer(swipe)
        snackbar?.bringSubview(toFront: (snackbar?.swipeView)!)
        
        let url = URL(string: track.image)
        
        DispatchQueue.global(qos: .userInitiated).async {
            MTFirestore.only.getMaptrack(forMaptrackId: track.maptrack, callback: { maptrack, error in
                if let error = error {
                    print(error.localizedDescription)
                }
                if let maptrack = maptrack, maptrack.image != "" {
                    let userImageUrl = URL(string: maptrack.image)
                    MTDownloader.downloadImage(url: userImageUrl!, callback: { image, err in
                        DispatchQueue.main.async {
                            if let err = err {
                                print(err.localizedDescription)
                            }
                            if let image = image {
                                self.snackbar?.userImage.image = image
                            }
                        }
                    })
                }
            })
            MTDownloader.downloadImage(url: url!, callback: { image, error in
                DispatchQueue.main.async {
                    if let err = error {
                        print(err.localizedDescription)
                        self.snackbar?.albumCover.backgroundColor = MTColor.pinkSheer
                    } else if let img = image {
                        self.snackbar?.albumCover.image = img
                        self.snackbar?.setNeedsLayout()
                    }
                    self.snackbar?.slideUp()
                }
            })
        }
    }
    
    @objc func snackbarAlbumImageAction(_ image: UIImage) {
        if let maptrackId = snackbar?.track.maptrack {
            DispatchQueue.global(qos: .userInitiated).async {
                MTFirestore.only.getMaptrack(forMaptrackId: maptrackId, callback: { maptrack, error in
                    if let err = error {
                        print(err.localizedDescription)
                    } else if let mt = maptrack {
                        DispatchQueue.main.async {
                            MTSessionData.only.addToMaptrackQueue(maptrack: mt)
                        }
                    }
                })
            }
        }
    }
    
    // handle swipe-down to close snackbar
    @objc func swipeAction(_ swipe: UISwipeGestureRecognizer) {
        print(swipe.direction.rawValue)
        switch (swipe.direction) {
        case UISwipeGestureRecognizerDirection.down:
            snackbar?.slideDownThen(callback: {
                self.snackbar = nil
            })
            if let line = displayLine {
                line.map = nil
            }
            break
        default:
            break
        }
    }
}

extension MapViewController: MaptrackTableDelegate {
    func handleTrackSelection(_ maptrackTableVC: MaptrackTableViewController, track: Track) {
        maptrackTableVC.dismiss(animated: true, completion: nil)
        handleTrackRouteView(track: track)
        handleTrackView(track: track)
    }
    
    @objc func handleSnackbarPlayButtonAction(_ button: PlaybackButton) {
        button.togglePlaying()
        if let track = button.track {
            if button.isPlaying {
                 MTSPTAudioController.only.startTrack(track: track)
            } else {
                MTSPTAudioController.only.pauseTrack()
            }
        }
    }
}

extension MapViewController: UIPopoverPresentationControllerDelegate {
    func launchMaptrackTablePopover(withTracks: [Track]) {
        /*
         thanks to Stefan for his popover view controller creation code
         https://github.com/FitnessEffect/WorkoutTracker/blob/master/WorkoutTracker/InputExerciseViewController.swift
         */
        let xPosition = view.frame.width / 2
        let yPosition = view.frame.height / 2
        let popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "maptrackTableVC") as! MaptrackTableViewController
        popover.delegate = self
        popover.modalPresentationStyle = UIModalPresentationStyle.popover
        popover.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popover.popoverPresentationController?.delegate = self
        popover.popoverPresentationController?.sourceView = view
        popover.preferredContentSize = CGSize(width: 260, height: 240)
        popover.popoverPresentationController?.sourceRect = CGRect(x: xPosition, y: yPosition, width: 0, height: 0)
        popover.tracks = withTracks
        present(popover, animated: true, completion: nil)
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension MapViewController: MTLocManagerDelegate {
    func snapToRoads(withLocations locations: [CLLocationCoordinate2D]) {
        MTMusicApp.only.getCurrentlyPlaying()
        
        MTSpotify.only.getCurrentlyPlaying()

        DispatchQueue.global().async {
            MTGoogleApi.only.snapPointsToRoads(withLocations: locations, callback: { snappedData, error in
                if let err = error {
                    print(err.localizedDescription)
                }

                if let data = snappedData {
                    self.handleSnappedPoints(fromResponse: data)
                }
            })
        }
    }

    func locationDidUpdate(newLocation location: CLLocationCoordinate2D) {
        if markerUpdated {
            markerUpdated = false
        } else {
            repositionMapMarker(toLocation: location)
        }
    }
    
}

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let mtClusterItem = marker.userData as? MTClusterItem {
            let camera = GMSCameraPosition.camera(withTarget: mtClusterItem.position, zoom: mapView.camera.zoom)
            let update = GMSCameraUpdate.setCamera(camera)
            mapView.animate(with: update)
            
            DispatchQueue.global(qos: .userInitiated).async {
                MTFirestore.only.getTrack(forTrackId: mtClusterItem.maptrack.track, callback: { track, error in
                    if let err = error {
                        print(err.localizedDescription)
                    }
                    if let t = track {
                        DispatchQueue.main.async {
                            self.handleTrackRouteView(track: t)
                            self.handleTrackView(track: t)
                        }
                    }
                })
            }
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if snackbar != nil {
            snackbar?.slideDownThen(callback: {
                self.snackbar = nil
            })
        }
        if let line = displayLine {
            line.map = nil
        }
    }
}

extension MapViewController: GMUClusterManagerDelegate {
    func clearRouteClusters() {
        clusterManager.clearItems()
    }
    
    func clearNearbyClusters() {
        if let line = displayLine {
            line.map = nil
        }

        for item in nearbyMarkers {
            clusterManager.remove(item)
        }
        
        nearbyMarkers.removeAll()

        clusterManager.cluster()
    }
    
    func clearHistoryClusters() {
        if let line = displayLine {
            line.map = nil
        }

        for item in historicalMarkers {
            clusterManager.remove(item)
        }
        
        historicalMarkers.removeAll()

        clusterManager.cluster()
    }
    
    func generateNearbyClusters(maptracks: [Maptrack]) {
        generateRouteClusterItems(maptracks: maptracks, queryType: .nearby)
        clusterManager.add(nearbyMarkers)
        clusterManager.cluster()
    }
    
    func generateHistoryClusters(maptracks: [Maptrack]) {
        generateRouteClusterItems(maptracks: maptracks, queryType: .history)
        clusterManager.add(historicalMarkers)
        clusterManager.cluster()
    }
    
    func generateRouteClusterItems(maptracks: [Maptrack], queryType: MTStoreQuery) {
        for item in maptracks {
            let newItem = MTClusterItem(
                position: CLLocationCoordinate2DMake(item.midpoint.lat, item.midpoint.long),
                maptrack: item
            )

            if queryType == .history {
                historicalMarkers.append(newItem)
            } else if queryType == .nearby {
                nearbyMarkers.append(newItem)
            }
        }
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let camera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: mapView!.camera.zoom)
        let update = GMSCameraUpdate.setCamera(camera)
        mapView?.animate(with: update)
        selectedTracks = getTrackIdsFromClusterItems(items: cluster.items as! [MTClusterItem])
        
        DispatchQueue.global(qos: .userInitiated).async {
            MTFirestore.only.getTracks(forTrackIds: self.selectedTracks, callback: { tracks, error in
                if let err = error {
                    print(err.localizedDescription)
                }
                if let t = tracks {
                    DispatchQueue.main.async {
                        self.launchMaptrackTablePopover(withTracks: t)
                        self.removeDisplayRouteLine()
                        self.removeSnackbar() {
                            return
                        }
                    }
                }
            })
        }
        return false
    }
    
    func getSpotifyIdsFromTracks(items: [Track]) -> [String] {
        return items.map({ item in
            return item.spotify
        })
    }
    
    func getTrackIdsFromClusterItems(items: [MTClusterItem]) -> [String] {
        return items.map({ item in
            return item.maptrack.track
        })
    }
}

extension MapViewController {
    func queryForMaptracks() {
        let region = mapView?.projection.visibleRegion()
        let maxLat = (region?.farLeft.latitude)!
        let minLong = (region?.farLeft.longitude)!
        let minLat = (region?.nearRight.latitude)!
        let maxLong = (region?.nearRight.longitude)!
        let min = CLLocationCoordinate2D(latitude: minLat, longitude: minLong)
        let max = CLLocationCoordinate2D(latitude: maxLat, longitude: maxLong)
        DispatchQueue.global(qos: .userInitiated).async {
            MTFirestore.only.getNearbyRoutes(min: min, max: max, callback: { nearbyRoutes, error in
                if let err = error {
                    print(err.localizedDescription)
                }
                if let routes = nearbyRoutes {
                    DispatchQueue.main.async {
                        self.generateNearbyClusters(maptracks: routes)
                    }
                }
            })
        }
    }
    
    func queryForHistory() {
        DispatchQueue.global(qos: .userInitiated).async {
            MTFirestore.only.getRoutesForUser(callback: { nearbyRoutes, error in
                if let err = error {
                    print(err.localizedDescription)
                }
                if let routes = nearbyRoutes {
                    DispatchQueue.main.async {
                        self.generateHistoryClusters(maptracks: routes)
                    }
                }
            })
        }
    }
}




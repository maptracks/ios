//
//  MTMPMusicPlayerController.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/18/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import MediaPlayer

class MTMPMusicPlayerController: NSObject {
    static let only = MTMPMusicPlayerController()
    
    private var _player: MPMusicPlayerController!
    private var _progress: TimeInterval!
    private var _currentTrack: Track?
    
    var isPlaying: Bool {
        get {
            if let p = _player {
                return p.playbackState == .playing
            }
            return false
        }
    }
    
    private override init() {
        super.init()
        _player = MPMusicPlayerController.systemMusicPlayer
        _progress = TimeInterval(0)
    }
    
    private func createQuery(track: Track) -> MPMediaQuery? {
        if track.apple.count == 0 { return nil }
        let predicate = MPMediaPropertyPredicate(value: track.apple, forProperty: MPMediaItemPropertyPersistentID)
        let query = MPMediaQuery()
        query.addFilterPredicate(predicate)
        return query
    }
    
    public func stopItem() {
        if _player.playbackState == .playing {
            _player.stop()
        }
    }
    
    public func playItem(track: Track) {
        if let query = createQuery(track: track) {
            if let items = query.items, items.count > 0 {
                _currentTrack = track
                _player.setQueue(with: query)
                _player.play()
            } else {
                print("no track found")
            }
        } else {
            print("no track found")
        }
    }
}

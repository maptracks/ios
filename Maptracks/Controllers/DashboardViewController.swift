//
//  DashboardViewController.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import FirebaseAuth
import SafariServices
import Instructions

class DashboardViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    
    var handle: AuthStateDidChangeListenerHandle?
    
    var loginUrl: URL?
    
    var dashView: DashboardView?
    var connectView: ConnectMusicView!
    
//    var coachMarksController: CoachMarksController!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAuthListener()
        setupSpotifyAuth()
        validateSpotifySession()
        MXSender.trackLoginEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MTSessionData.only.initialLocation = nil
        setupNotifications()
        setupLocationManager()
        validateSpotifySession()
        initUi()
    }
    
    func setupLocationManager() {
        MTLocManager.only.delegate = self
        MTLocManager.only.requestLocationPermissions()
    }
    
    func setupAuthListener() {
        if handle != nil {
            Auth.auth().removeStateDidChangeListener(handle!)
        }
        handle = Auth.auth().addStateDidChangeListener { auth, user in
            if user == nil {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateAfterFirstSpotifyLogin), name: Notification.Name(rawValue: "spotifyLoginSuccess"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleSpotifyPlayingNotification), name: Notification.Name(rawValue: "spotifyCurrentlyPlaying"), object: MTSpotify.only)
        NotificationCenter.default.addObserver(self, selector: #selector(handleMusicAppPlayingNotification), name: Notification.Name(rawValue: "musicAppCurrentlyPlaying"), object: MTMusicApp.only)
    }
    
    @objc func handleSpotifyPlayingNotification(_ notification: Notification) {
        guard let data = notification.userInfo as? [String: Any],
            let track = data["track"] as? Track,
            let progress = data["progress"] as? Double,
            let isPlaying = data["isPlaying"] as? Bool,
            let timestamp = data["timestamp"] as? Double else { return }
        if isPlaying {
            self.handleSpotifyCurrentlyPlaying(track: track, progress: progress, timestamp: timestamp)
        }
    }
    
    @objc func handleMusicAppPlayingNotification(_ notification: Notification) {
        guard let data = notification.userInfo as? [String: Any],
            let track = data["track"] as? Track,
            let progress = data["progress"] as? Double,
            let isPlaying = data["isPlaying"] as? Bool,
            let timestamp = data["timestamp"] as? Double else { return }
        if isPlaying {
            self.handleMusicAppCurrentlyPlaying(track: track, progress: progress, timestamp: timestamp)
        }
    }
    
    func teardownNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "spotifyCurrentlyPlaying"), object: MTSpotify.only)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "musicAppCurrentlyPlaying"), object: MTMusicApp.only)
    }
    
    func initUi() {
        initDashView()
        if defaults.object(forKey: "HasLaunchedBefore") != nil {
//            coachMarksController = CoachMarksController()
//            coachMarksController.dataSource = self
//            coachMarksController.overlay.blurEffectStyle = UIBlurEffectStyle.dark
//            coachMarksController.start(on: self)
            defaults.set(true, forKey: "HasLaunchedBefore")
            defaults.synchronize()
        }
    }
    
    func initDashView() {
        let topInset = LayoutUtils.getTopInset()
        let bottomInset = LayoutUtils.getBottomInset()
        let frame = CGRect(x: 0, y: topInset, width: view.frame.width, height: view.frame.height - (topInset + bottomInset))
        dashView = DashboardView.instanceFromNib(frame: frame)
        dashView?.delegate = self
        view.addSubview(dashView!)
        dashView?.initUi()
    }
    
    func initConnectView() {
        connectView = ConnectMusicView.instanceFromNib(frame: view.frame)
        connectView.delegate = self
        view.addSubview(connectView)
        connectView.spotifyConnected = MTSpotify.only.isConnected
        connectView.musicAppConnected = MTMusicApp.only.isAuthorized
        connectView.display()
    }
    
    func setupHistoryView() {
        DispatchQueue.global(qos: .userInitiated).async {
            MTFirestore.only.getRoutesForUser(callback: { maptracks, error in
                if let err = error { print(err.localizedDescription) }
                if let mt = maptracks {
                    DispatchQueue.main.async {
                        let history = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "history") as! HistoryTableViewController
                        history.maptracks = mt
                        self.present(history, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    func setupMapView() {
        teardownNotifications()
        let map = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "map") as! MapViewController
        present(map, animated: false, completion: nil)
    }
    
    func handleLogout() {
        do {
            if let domain = Bundle.main.bundleIdentifier {
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
            }
            try Auth.auth().signOut()
        } catch let err {
            print(err.localizedDescription)
        }
    }
    
    func setupSpotifyAuth() {
        let auth = SPTAuth.defaultInstance()

        auth?.clientID = "a568fd7163b44e5881e1d4ebcaeacd85"
        auth?.redirectURL = URL(string: "maptracks://spotify-login")
        auth?.sessionUserDefaultsKey = "current session"
        auth?.tokenSwapURL = URL(string: "https://maptracks.me/api/swap")
        auth?.tokenRefreshURL = URL(string: "https://maptracks.me/api/refresh")
        auth?.requestedScopes = [SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope, SPTAuthUserLibraryReadScope, SPTAuthUserReadPrivateScope, SPTAuthPlaylistReadCollaborativeScope, MTSPT.MTSPTAuthCurrentlyPlayingScope, MTSPT.MTSPTAuthRecentlyPlayedScope]

        loginUrl = auth?.spotifyWebAuthenticationURL()
    }
    
    func validateSpotifySession() {
        let auth = SPTAuth.defaultInstance()

        if let session = auth?.session {
            if session.isValid() {
                MTSPTAudioController.only.initAudioStreaming()
                MTSpotify.only.accessToken = session.accessToken
            } else if (auth?.hasTokenRefreshService)! {
                renewSpotifySession()
            }
        }

        if let connect = connectView {
            connect.spotifyConnected = MTSpotify.only.isConnected
        }
    }
    
    func renewSpotifySession() {
        let auth = SPTAuth.defaultInstance()

        auth?.renewSession(auth?.session, callback: { error, session in
            if let err = error {
                print(err.localizedDescription)
            }

            if let sess = session {
                auth?.session = sess
                MTSPTAudioController.only.initAudioStreaming()
                MTSpotify.only.accessToken = sess.accessToken
            }

            if let connect = self.connectView {
                connect.spotifyConnected = MTSpotify.only.isConnected
            }
        })
    }
    
    @objc func updateAfterFirstSpotifyLogin () {
        let auth = SPTAuth.defaultInstance()

        if let sessionObj: AnyObject = defaults.object(forKey: "SpotifySession") as AnyObject? {
            let sessionDataObj = sessionObj as! Data
            let firstTimeSession = NSKeyedUnarchiver.unarchiveObject(with: sessionDataObj) as! SPTSession

            auth?.session = firstTimeSession

            MTSpotify.only.accessToken = firstTimeSession.accessToken
            MTSPTAudioController.only.initAudioStreaming()
        }
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func spotifyAuthViewController() -> SFSafariViewController{
        let svc = SFSafariViewController(url: loginUrl!)
        svc.delegate = self
        svc.modalPresentationStyle = UIModalPresentationStyle.pageSheet
        return svc
    }
}

extension DashboardViewController: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("done authorizing")
    }
}

extension DashboardViewController: DashboardViewDelegate {
    func stopCoachController() {
//        coachMarksController.stop(immediately: true)
    }

    func displayHistoryController() {
        stopCoachController()
        setupHistoryView()
    }
    
    func displayMapController() {
        stopCoachController()
        setupMapView()
    }

    func displayConnectController() {
        stopCoachController()
        initConnectView()
    }
    
    func logout() {
        stopCoachController()
        handleLogout()
    }
}

extension DashboardViewController: ConnectMusicViewDelegate {
    func goBack() {
        connectView.remove()
        connectView = nil
    }
    
    func authenticateSpotify() {
        let auth = SPTAuth.defaultInstance()
        if SPTAuth.supportsApplicationAuthentication() {
            UIApplication.shared.open((auth?.spotifyAppAuthenticationURL())!, options: [:], completionHandler: nil)
        } else {
            let svc = spotifyAuthViewController()
            present(svc, animated: true, completion: nil)
        }
    }
    
    func isSpotifyConnected() -> Bool {
        return MTSpotify.only.isConnected
    }
    
    func authenticateAppleMusic() -> Bool {
        return false
    }
    
    func isAppleMusicConnected() -> Bool {
        return false
    }
    
    func isMusicAppConnected() -> Bool {
        return MTMusicApp.only.isAuthorized
    }
    
    func authenticateMusicApp() {
        MTMusicApp.only.requestAuthorization(callback: { success in
            DispatchQueue.main.async {
                if let connect = self.connectView {
                    connect.musicAppConnected = success
                }
            }
        })
    }
}

extension DashboardViewController: MTLocManagerDelegate {
    // handle after retrieving a currently playing song from the music app
    // this is retrieved from the notification observer in setupNotifications()
    func handleMusicAppCurrentlyPlaying(track: Track, progress: Double, timestamp: Double) {
        if MTSessionData.only.activeTrack == nil {
            MTSessionData.only.activeTrack = track
            MTSessionData.only.trackStart = timestamp - progress
            MTSessionData.only.trackProgress = progress
        } else if MTSessionData.only.activeTrack?.apple == track.apple && progress >= MTSessionData.only.trackProgress! {
            MTSessionData.only.trackProgress = progress
        } else {
            print("dash saving music app currently playing")

            MTSessionData.only.tracks.append(MTSessionData.only.activeTrack!)
            
            if let route = MTSessionData.only.activeRoute {
                var maxLat: Double = -180
                var maxLong: Double = -180
                var minLat: Double = 180
                var minLong: Double = 180
                
                for pt in route.waypoints {
                    if pt.lat > maxLat {
                        maxLat = pt.lat
                    }
                    if pt.long > maxLong {
                        maxLong = pt.long
                    }
                    if pt.lat < minLat {
                        minLat = pt.lat
                    }
                    if pt.long < minLong {
                        minLong = pt.long
                    }
                }
                
                let max = Waypoint(lat: maxLat, long: maxLong)
                let min = Waypoint(lat: minLat, long: minLong)
                
                let midLat: Double = max.lat - ((max.lat - min.lat) / 2)
                let midLong: Double = max.long - ((max.long - min.long) / 2)
                let mid = Waypoint(lat: midLat, long: midLong)
                
                let maptrackId = MTFirestore.only.newMaptrackId()
                let routeId = MTSessionData.only.activeRoute!.id
                let trackId = MTSessionData.only.activeTrack!.id
                
                let newMaptrack = Maptrack(id: maptrackId, comment: "", image: "", start: MTSessionData.only.routeStart, end: timestamp, track: trackId, user: MTSessionData.only.uid, route: routeId, midpoint: mid, minpoint: min, maxpoint: max)
                
                MTFirestore.only.save(route: route, id: routeId, maptrackId: maptrackId)
                MTFirestore.only.save(track: MTSessionData.only.activeTrack!, id: trackId, maptrackId: maptrackId)
                
                MTFirestore.only.save(maptrack: newMaptrack, id: maptrackId)
                
                MTSessionData.only.routes.append(route)
            }
            
            MTSessionData.only.activeTrack = track
            MTSessionData.only.activeRoute = nil
            MTSessionData.only.waypoints.removeAll()
            MTSessionData.only.trackProgress = progress
            MTSessionData.only.trackStart = timestamp - progress
        }
    }
    
    // handle after retrieving a currently playing song from spotify
    // this is retrieved from the notification observer in setupNotifications()
    func handleSpotifyCurrentlyPlaying(track: Track, progress: Double, timestamp: Double) {
        if MTSessionData.only.activeTrack == nil {
            MTSessionData.only.activeTrack = track
            MTSessionData.only.trackStart = timestamp - progress
            MTSessionData.only.trackProgress = progress
        } else if MTSessionData.only.activeTrack?.spotify == track.spotify && progress >= MTSessionData.only.trackProgress! {
            MTSessionData.only.trackProgress = progress
        } else {
            print("dash saving spotify currently playing")
            MTSessionData.only.tracks.append(MTSessionData.only.activeTrack!)
            
            if let route = MTSessionData.only.activeRoute {
                var maxLat: Double = -180
                var maxLong: Double = -180
                var minLat: Double = 180
                var minLong: Double = 180
                
                for pt in route.waypoints {
                    if pt.lat > maxLat {
                        maxLat = pt.lat
                    }
                    if pt.long > maxLong {
                        maxLong = pt.long
                    }
                    if pt.lat < minLat {
                        minLat = pt.lat
                    }
                    if pt.long < minLong {
                        minLong = pt.long
                    }
                }
                
                let max = Waypoint(lat: maxLat, long: maxLong)
                let min = Waypoint(lat: minLat, long: minLong)
                
                let midLat: Double = max.lat - ((max.lat - min.lat) / 2)
                let midLong: Double = max.long - ((max.long - min.long) / 2)
                let mid = Waypoint(lat: midLat, long: midLong)
                
                let maptrackId = MTFirestore.only.newMaptrackId()
                let routeId = MTSessionData.only.activeRoute!.id
                let trackId = MTSessionData.only.activeTrack!.id
                
                let newMaptrack = Maptrack(id: maptrackId, comment: "", image: "", start: MTSessionData.only.routeStart, end: timestamp, track: trackId, user: MTSessionData.only.uid, route: routeId, midpoint: mid, minpoint: min, maxpoint: max)
                
                MTFirestore.only.save(route: route, id: routeId, maptrackId: maptrackId)
                MTFirestore.only.save(track: MTSessionData.only.activeTrack!, id: trackId, maptrackId: maptrackId)
                
                MTFirestore.only.save(maptrack: newMaptrack, id: maptrackId)
                
                MTSessionData.only.routes.append(route)
            }
            
            MTSessionData.only.activeTrack = track
            MTSessionData.only.activeRoute = nil
            MTSessionData.only.waypoints.removeAll()
            MTSessionData.only.trackProgress = progress
            MTSessionData.only.trackStart = timestamp - progress
        }
    }
    
    // handle retrieving snap-to-roads locations
    // NOTE: called from a separate thread, return to main thread is in method
    func handleSnappedPoints(fromResponse snappedData: [[String: Any]]) {
        let waypoints = GApiDataExtractor.generateWaypoints(fromSnappedData: snappedData)
        
        let now = Date().timeIntervalSince1970
        
        if MTSessionData.only.routeStart == 0 {
            MTSessionData.only.routeStart = now
        }
        
        MTSessionData.only.waypoints.append(contentsOf: waypoints)
        
        if MTSessionData.only.activeRoute == nil {
            let routeId = MTFirestore.only.newRouteId()
            MTSessionData.only.activeRoute = Route(id: routeId, maptrack: "", waypoints: MTSessionData.only.waypoints)
        } else {
            MTSessionData.only.activeRoute?.waypoints.append(contentsOf: waypoints)
            MTSessionData.only.routeEnd = now
        }
    }
    
    func locationDidUpdate(newLocation location: CLLocationCoordinate2D) {
        if MTSessionData.only.initialLocation == nil {
            MTSessionData.only.initialLocation = location
        }
    }
    
    func snapToRoads(withLocations locations: [CLLocationCoordinate2D]) {
        MTMusicApp.only.getCurrentlyPlaying()

        MTSpotify.only.getCurrentlyPlaying()
        
        MTGoogleApi.only.snapPointsToRoads(withLocations: locations, callback: { snappedData, error in
            if let err = error {
                print(err.localizedDescription)
            }
            
            if let data = snappedData {
                self.handleSnappedPoints(fromResponse: data)
            }
        })
    }
}

//extension DashboardViewController: CoachMarksControllerDelegate, CoachMarksControllerDataSource {
//    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
//        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, arrowOrientation: coachMark.arrowOrientation)
//
//        let hints = ["Connect to Spotify or your device's Music app", "Map your tracks, and discover music for your route", "See your history, and add a pic for any song"]
//
//        let nexts = ["Got It", "Cool", "Done"]
//
//        coachViews.bodyView.hintLabel.text = hints[index]
//        coachViews.bodyView.hintLabel.textContainer.lineBreakMode = .byTruncatingTail
//        coachViews.bodyView.nextLabel.text = nexts[index]
//
//        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
//    }
//
//    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkAt index: Int) -> CoachMark {
//        let buttons = [dashView!.settings, dashView!.map, dashView!.history]
//        return coachMarksController.helper.makeCoachMark(for: buttons[index])
//    }
//
//
//    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
//        return 3
//    }
//    
//}


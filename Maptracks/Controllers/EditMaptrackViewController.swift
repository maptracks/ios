//
//  EditMaptrackViewController.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/31/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import ImageIO

class EditMaptrackViewController: UIViewController, UINavigationControllerDelegate {
    var maptrack: Maptrack!
    var track: Track!
    var editView: EditMaptrackView?
    var imagePicker: UIImagePickerController!

    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        setupEditView()
    }
    
    func setupEditView() {
        editView = EditMaptrackView.instanceFromNib(frame: view.frame)
        editView?.maptrack = maptrack
        editView?.backButton.addTarget(self, action: #selector(closeEditAction), for: .touchUpInside)
        editView?.uploadImageButton.addTarget(self, action: #selector(chooseImageAction), for: .touchUpInside)
        editView?.titleLabel.text = track.title
        editView?.artistLabel.text = track.artist
        if maptrack.comment.count > 0 {
            editView?.commentView.text = maptrack.comment
        }
        MTDownloader.downloadImage(url: URL(string: track.image)!, callback: { image, error in
            self.editView?.albumCover.image = image
        })
        view.addSubview(editView!)
        setupUserImage()
        editView?.slideInLeft(callback: { _ in
        })
    }
    
    func setupUserImage() {
        if !maptrack.image.isEmpty {
            DispatchQueue.global().async {
                print(self.maptrack.image)
                let url = URL(string: self.maptrack.image)
                MTDownloader.downloadImage(url: url!, callback: { image, error in
                    DispatchQueue.main.async {
                        if let err = error {
                            print(err.localizedDescription)
                        }
                        if let image = image {
                            self.editView?.userImageView.contentMode = .scaleAspectFit
                            self.editView?.userImageView.image = image
                            self.editView?.uploadImageButton.backgroundColor = UIColor.clear
                        }
                    }
                })
            }
        }
    }
    
    @objc func closeEditAction() {
        if let text = editView?.commentView.text {
            let offset = text.count < 140 ? text.count : 140
            let endIndex = text.index(text.startIndex, offsetBy: offset)
            let comment = String(text[..<endIndex])
            MTFirestore.only.saveComment(maptrack: maptrack.id, comment: comment)
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func saveAction(_ button: UIButton) {
        print(editView!.commentView.text)
        print(editView!.maptrack)
    }
    
    @objc func chooseImageAction(_ button: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }

}

extension EditMaptrackViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selection = info[UIImagePickerControllerOriginalImage] as! UIImage
//        editView?.userImageView.contentMode = .scaleAspectFill
//        editView?.userImageView.image = selection
        picker.dismiss(animated: true, completion: nil)
        let rsk = RSKImageCropViewController(image: selection, cropMode: .square)
        rsk.delegate = self
        present(rsk, animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension EditMaptrackViewController: RSKImageCropViewControllerDelegate {
    func resizeImageThenSave(atUrl url: URL, toSize size: CGSize) {
        if let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) {
            let options: [NSString: NSObject] = [
                kCGImageSourceThumbnailMaxPixelSize: max(size.width, size.height) as NSObject,
                kCGImageSourceCreateThumbnailFromImageAlways: true as NSObject
            ]
            
            let scaledImage = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options as CFDictionary).flatMap({ UIImage(cgImage: $0) })
            
            if let imageData = UIImagePNGRepresentation(scaledImage!) {
                MTStorage.only.uploadMaptrackUserImage(data: imageData, maptrackId: maptrack.id, callback: { downloadUrl, error in
                    if let err = error {
                        print(err.localizedDescription)
                    }
                    if let downloadUrl = downloadUrl {
                        MTFirestore.only.updateMaptrackImage(maptrackId: self.maptrack.id, downloadUrl: downloadUrl)
                        MTDownloader.cacheImage(scaledImage, atUrlString: downloadUrl)
                    }
                })
            }
        }
    }

    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        editView?.userImageView.contentMode = .scaleAspectFit
        editView?.userImageView.image = croppedImage
        editView?.uploadImageButton.backgroundColor = UIColor.clear
        controller.dismiss(animated: false, completion: nil)
        let url = URL(fileURLWithPath: NSTemporaryDirectory().appending("tempImage.png"))
        do {
            try UIImagePNGRepresentation(croppedImage)?.write(to: url)
            let size = CGSize(width: croppedImage.size.width / 2, height: croppedImage.size.height / 2)
            resizeImageThenSave(atUrl: url, toSize: size)
        } catch let err {
            print(err.localizedDescription)
        }
    }
}








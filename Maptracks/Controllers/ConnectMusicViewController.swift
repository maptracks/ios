//
//  ConnectMusicViewController.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/12/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

class ConnectMusicViewController: UIViewController {
    var connectView: ConnectMusicView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupConnectView()
    }

    func setupConnectView() {
        connectView = ConnectMusicView.instanceFromNib(frame: view.frame)
//        connectView.delegate = self
        view.addSubview(connectView)
    }
}

extension ConnectMusicViewController {
    func goBack() {
        dismiss(animated: true, completion: nil)
    }
}

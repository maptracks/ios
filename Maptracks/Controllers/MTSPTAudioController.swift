//
//  MTSPTAudioController.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/6/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import Foundation
import AVFoundation

class MTSPTAudioController: NSObject {
    static let only = MTSPTAudioController()

    private var _player: SPTAudioStreamingController!
    private var _progress: TimeInterval!
    private var _currentTrack: Track?
    private var _avSession: AVAudioSession!
    
    private override init() {
        super.init()
        _player = SPTAudioStreamingController.sharedInstance()
        _avSession = AVAudioSession.sharedInstance()
        _progress = TimeInterval(0)
    }
    
    var isPlaying: Bool {
        get {
            if let p = _player {
                if let state = p.playbackState {
                    return state.isPlaying
                }
            }
            return false
        }
    }
    
    var isPaused: Bool {
        get {
            if let p = _player {
                return p.playbackState.isPlaying && p.playbackState.position > 0
            }
            return false
        }
    }
    
    var isStopped: Bool {
        get {
            if let p = _player {
                return !p.initialized
            }
            return true
        }
    }
    
    func startTrack(uri: String) {
        if uri.count == 0 { return }
        if let p = _player {
            p.playSpotifyURI(uri, startingWith: 0, startingWithPosition: 0, callback: { error in
                if let err = error {
                    print(err.localizedDescription)
                }
            })
        }
    }
    
    func startTrack(track: Track) {
        _currentTrack = track
        if track.spotify.count == 0 { return }
        if let p = _player {
            let uri = track.audio
            p.playSpotifyURI(uri, startingWith: 0, startingWithPosition: 0, callback: { error in
                if let err = error {
                    print(err.localizedDescription)
                }
            })
        }
    }
    
    func resumeTrack() {
        if let p = _player {
            if let track = _currentTrack {
                if track.spotify.count == 0 { return }
                p.playSpotifyURI(track.audio, startingWith: 0, startingWithPosition: _progress, callback: { error in
                    if let err = error {
                        print(err.localizedDescription)
                    }
                })
            } else {
                p.setIsPlaying(true, callback: { error in
                    if let err = error {
                        print(err.localizedDescription)
                    }
                })
            }
        }
    }
    
    func pauseTrack() {
        if let p = _player {
            _progress = p.playbackState.position
            p.setIsPlaying(false, callback: { error in
                if let err = error {
                    print(err.localizedDescription)
                }
            })
        }
    }
    
    func stopTrack() {
        if let p = _player {
            p.setIsPlaying(false, callback: { error in
                if let err = error {
                    print(err.localizedDescription)
                }
            })
            _currentTrack = nil
            _progress = TimeInterval(0)
        }
    }
    
    @objc func initAudioStreaming() {
        let auth = SPTAuth.defaultInstance()

        if let session = auth?.session {
            if session.isValid() {
                if let p = _player {
                    if !(p.loggedIn) && !(p.initialized) {
                        do {
                            try p.start(withClientId: auth?.clientID, audioController: nil, allowCaching: true)
                            p.login(withAccessToken: session.accessToken)
                        } catch let err {
                            print (err.localizedDescription)
                        }
                        _player?.delegate = self
                        _player?.playbackDelegate = self
                        _player?.diskCache = SPTDiskCache(capacity: 1024 * 1024 * 64)
                    }
                }
            }
        }
    }
}

extension MTSPTAudioController: SPTAudioStreamingPlaybackDelegate {
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didReceive event: SpPlaybackEvent) {
        switch (event) {
        case SPPlaybackNotifyPlay:
            print("playback play event")
            break
        case SPPlaybackNotifyBecameActive:
            print("playback became active event")
            break
        case SPPlaybackNotifyNext:
            print("playback next track event")
            break
        case SPPlaybackNotifyPause:
            print("playback pause event")
            break
        default:
            print(event.rawValue)
            break
        }
    }
}

extension MTSPTAudioController: SPTAudioStreamingDelegate {
    func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
        print("audio streaming logged in")
    }
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didChangePlaybackStatus isPlaying: Bool) {
        DispatchQueue.global().async {
            if (isPlaying) {
                do {
                    try self._avSession.setCategory(AVAudioSessionCategoryPlayback)
                    try self._avSession.setActive(true)
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                do {
                    try self._avSession.setActive(false)
                } catch let error {
                    print(error.localizedDescription)
                }
            }
        }
    }
}

//
//  MaptrackTableViewController.swift
//  Maptracks
//
//  Created by Emily Kolar on 12/4/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit

protocol MaptrackTableDelegate {
    func handleTrackSelection(_ maptrackTableVC: MaptrackTableViewController, track: Track) -> Void
}

class MaptrackTableViewController: UITableViewController {
    var delegate: MaptrackTableDelegate?
    
    var tracks = [Track]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MaptrackCell", for: indexPath) as! MaptrackCell

        cell.tag = indexPath.row

        if cell.playButton == nil {
            cell.playButton = PlaybackButton.instanceFromNib(x: cell.albumImage.frame.origin.x, y: cell.albumImage.frame.origin.y)
            cell.addSubview(cell.playButton!)
        }

        cell.nameLabel.text = tracks[indexPath.row].title
        cell.artistLabel.text = tracks[indexPath.row].artist
        cell.playButton!.trackIndex = indexPath.row
        cell.playButton!.track = tracks[indexPath.row]
        cell.playButton!.addTarget(self, action: #selector(playButtonAction(_:)), for: .touchUpInside)
        cell.albumImage.image = nil

        let url = URL(string: tracks[indexPath.row].image)

        DispatchQueue.global(qos: .userInitiated).async {
            MTDownloader.downloadImage(url: url!, callback: { image, error in
                if let err = error {
                    print(err.localizedDescription)
                } else if let img = image {
                    DispatchQueue.main.async {
                        if cell.tag == indexPath.row {
                            cell.albumImage.image = img
                        }
                    }
                }
            })
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.handleTrackSelection(self, track: tracks[indexPath.row])
    }
    
    @objc func playButtonAction(_ button: PlaybackButton) {
        if let track = button.track {
            if button.isPlaying {
                if track.spotify != "" && MTSpotify.only.isConnected {
                    MTSPTAudioController.only.pauseTrack()
                     button.togglePlaying()
                } else if track.apple != "" && MTMusicApp.only.isAuthorized {
                    MTMPMusicPlayerController.only.stopItem()
                     button.togglePlaying()
                }
            } else {
                if track.spotify != "" && MTSpotify.only.isConnected {
                    MTSPTAudioController.only.startTrack(track: tracks[button.trackIndex])
                     button.togglePlaying()
                } else if track.apple != "" && MTMusicApp.only.isAuthorized {
                    MTMPMusicPlayerController.only.playItem(track: track)
                     button.togglePlaying()
                }
            }
        }
    }
}

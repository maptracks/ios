//
//  AuthViewController.swift
//  Maptracks
//
//  Created by Emily Kolar on 11/3/17.
//  Copyright © 2017 Emily Kolar. All rights reserved.
//

import UIKit
import CircleMenu
import FirebaseAuth
import SafariServices
import FirebaseFirestore

class AuthViewController: UIViewController, CircleMenuDelegate {
    var handle: AuthStateDidChangeListenerHandle?
    
    var dashboard: DashboardViewController?
    var startView: StartView?
    var loginView: LoginView?
    var signupView: SignUpView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setAuthHandler()
        initUi()
    }
    
    func setAuthHandler() {
        if handle != nil {
            Auth.auth().removeStateDidChangeListener(handle!)
        }
        handle = Auth.auth().addStateDidChangeListener { auth, user in
            if let u = user {
                MTSessionData.only.uid = u.uid
                self.getUserForMixpanelProfile(uid: u.uid)
                self.cleanupViews()
                self.presentDashboard()
            } else {
                self.initStartView()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let authHandle = handle {
            Auth.auth().removeStateDidChangeListener(authHandle)
        }
    }
    
    func initUi() {
        initStartView()
    }
    
    func initStartView() {
        startView = StartView.instanceFromNib(frame: view.frame)
        startView?.delegate = self
        view.addSubview(startView!)
        startView?.display()
    }
    
    func initLoginView() {
        loginView = LoginView.instanceFromNib(frame: view.frame)
        loginView?.delegate = self
        view.addSubview(loginView!)
        loginView?.initUi()
    }
    
    func initSignupView() {
        signupView = SignUpView.instanceFromNib(frame: view.frame)
        signupView?.delegate = self
        view.addSubview(signupView!)
        signupView?.initUi()
    }
    
    func cleanupViews() {
        loginView?.remove()
        signupView?.remove()
        startView?.remove()
        loginView = nil
        signupView = nil
        startView = nil
    }
    
    func presentDashboard() {
        self.cleanupViews()
        if let h = handle {
            Auth.auth().removeStateDidChangeListener(h)
        }
        dashboard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "dashboard") as? DashboardViewController
        present(dashboard!, animated: true, completion: nil)
    }
}

extension AuthViewController: StartViewDelegate {
    func handleChangeTo(_ authMode: AuthMode) {
        startView?.remove()
        
        switch (authMode) {
        case .signup:
            initSignupView()
            break
        case .login:
            initLoginView()
            break
        }
    }
}

extension AuthViewController: LoginViewDelegate {
    func handleSwitchToSignUpMode() {
        loginView?.remove()
        initSignupView()
        loginView = nil
    }

    func authenticateUser(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { user, _ in
            if let u = user {
                let alias = UserDefaults.standard.string(forKey: "MixpanelAlias")
                if alias == nil {
                    MXSender.setAlias(uid: u.uid)
                    self.getUserForMixpanelProfile(uid: u.uid)
                }
            }
        })
    }
}

extension AuthViewController: SignUpViewDelegate {
    func handleSwitchToLoginMode() {
        signupView?.remove()
        initLoginView()
        signupView = nil
    }
    
    func createUser(fullName: String, email: String, password: String) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { user, error in
            if let err = error {
                print(err.localizedDescription)
            }

            if let user = user {
                let timestamp = FieldValue.serverTimestamp()

                var userData: [String: Any] = [
                    "uid": user.uid,
                    "fullName": fullName,
                    "email": user.email!,
                    "created": timestamp,
                    "updated": timestamp,
                    "photoUrl": ""
                ]
                
                if let url = user.photoURL {
                    userData.updateValue(url.absoluteString, forKey: "photoUrl")
                }
                
                self.changeRequest(firebaseUser: user, displayName: fullName)
                MTFirestore.only.save(fireUser: user, userData: userData)
                MXSender.setAlias(uid: user.uid)
                self.getUserForMixpanelProfile(uid: user.uid)
                MXSender.trackCreateUserEvent()
            }
        })
    }
    
    func launchPrivacyPolicyBrowser() {
        let url = "https://maptracks.me/privacy"
        let ctrl = SFSafariViewController(url: URL(string: url)!)
        present(ctrl, animated: true, completion: nil)
    }
}

extension AuthViewController {
    func changeRequest(firebaseUser user: User, displayName name: String) {
        let changeRequest = user.createProfileChangeRequest()
        changeRequest.displayName = name
        changeRequest.commitChanges(completion: { error in
            if let e = error {
                print(e.localizedDescription)
            }
        })
    }
    
    func getUserForMixpanelProfile(uid: String) {
        DispatchQueue.global().async {
            MTFirestore.only.getUser(uid: uid, callback: { data, error in
                DispatchQueue.main.async {
                    if let err = error { print(err.localizedDescription) }
                    if let d = data {
                        let fullName = d["fullName"] as! String
                        let email = d["email"] as! String
                        let uid = d["uid"] as! String
                        let photoUrl = d["photoUrl"] as! String
                        MXSender.setPersonProperties(properties: ["$name": fullName, "$email": email, "uid": uid, "photoUrl": photoUrl])
                    }
                }
            })
        }
    }
}


